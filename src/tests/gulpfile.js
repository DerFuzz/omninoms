var gulp = require('gulp');
var mocha = require('gulp-mocha');
var babel_mocha = require('babel/register');

gulp.task('test', function () {
  return gulp.src('src/**/test*.js', {read: false})
  .pipe(mocha({
    compilers: {
      js: babel_mocha
    }
  }));
});

gulp.task('default', ['test']);
