import fs from 'fs';
import range from './range';

var locations = JSON.parse(fs.readFileSync('./addresses.json'));

export function getLocation(){
  var index = Math.round((Math.random() * 99), 2);
  return locations[index];
}

export function getItem() {
  return Math.round((Math.random() * 50), 2) + 1;
}

export function getMeal() {
  return {
    item: getItem(),
    quantity: Math.round((Math.random() * 3), 2) + 1,
    requests: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a diam lectus. Sed sit amet ipsum mauris."
  };
}

export function getFranchise() {
  return { id: 0, name: "Mc Donalds" };
}

export function getOwners() {
  return [{ user: { first_name: 'Fake', last_name: 'User' }, phone: '123-456-7890' }];
}

export function getOrder() {
  return {
    location: getLocation(),
    meals: [getMeal()],
    franchise: getFranchise(),
    owners: getOwners()
  };
}
