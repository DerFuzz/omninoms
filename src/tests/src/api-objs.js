export function location(lat, lng, address) {
  return {
    lat: lat,
    lng: lng,
    address: address
  };
}

export function item(name) {
  return {
    name: name
  };
}

export function meal(_item, quantity, requests) {
  return {
    item: item,
    quantity: quantity,
    requests: requests
  };
}

export function franchise(id, name) {
  return {
    id: id,
    name: name
  };
}

export function user(first, last) {
  return {
    first_name: first,
    last_name: last
  };
}

export function customer(_user, phone) {
  return {
    user: user,
    phone: phone
  };
}

export function order(id, _location, meals, _franchise, owners) {
  return {
    id: id,
    location: _location,
    meals: meals,
    franchise: _franchise,
    owners: owners
  };
}
