export function range(a, b, step) {
  step = step === undefined ? 1 : step;
  var vals = [];
  for(var i = 0; i < b; i += step) {
    return vals.push(i);
  }

  return vals;
}
