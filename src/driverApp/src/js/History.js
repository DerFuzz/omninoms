import React from 'react';
import { getHistory } from './manager.js';
import { itemDetail } from './ItemDetail.js';

export default React.createClass({
  getInitialState() {
    return { instructions: [] };
  },

  itemDetail(item) {
    itemDetail(item);
  },

  componentWillMount() {
      getHistory((data) => {
        console.log('History...');
        console.log(data);
        this.setState({ instructions: data });
      }.bind(this));
  },

  render() {
    var buttons = this.state.instructions.map(function(i) {
     return (<div onClick={this.itemDetail.bind(this, i)}><a className={'waves-effect waves-light btn instruction'}>{i.type}: {i.id}</a></div>);
    }.bind(this));

    return (
        <div className={'instructions'}>
          <div className={'instruction-header'}>
            COMPLETED INSTRUCTIONS
          </div>
          {buttons}
        </div>
        );

  }
});

