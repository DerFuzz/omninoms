import request from 'request-promise';
import poly from 'polyline';
import {OSRM_IP, auth} from './config.js';
import { itemDetail } from './ItemDetail.js';
import {getCurrent, onNext, onManifest, getDeliveries, getPickups} from './manager.js';
import {onLocation} from './geolocation.js';
import {getToken} from './login.js';

var React = require('react');

var polyline;

var clearCallback = function(){};
export function triggerClear() {
    clearCallback();
}

var drawCallback = function() {};
export function triggerDraw() {
    drawCallback();
}

var markerCallback = function() {};
export function triggerInstructionMarker() {
    markerCallback();
}

export default React.createClass({
  componentDidMount: function() {
    var locations = {
      'St. Catherines': [43.117507, -79.247641],
      'Windsor': [42.3, -83.03]
    };

    window.L.mapbox.accessToken = 'pk.eyJ1IjoiZnV6eiIsImEiOiI5OTUxOTU3NjgzODcwZGRiYzhmNGQzZTFmMWU0YjEwMSJ9.WKpEYK7DCfQbqlORyIVJQQ';
    var map = window.L.mapbox.map('mapMount', 'mapbox.streets', {zoomControl: false}).setView(locations.Windsor, 12);
    new window.L.Control.Zoom({ position: 'bottomright' }).addTo(map);

    var driverMarker;
    polyline = window.L.polyline([]).addTo(map);

    var instructionMarker;
    markerCallback = () => {
      if(instructionMarker !== undefined) {
        map.removeLayer(instructionMarker);
      }
      var inst = getCurrent();
      instructionMarker = window.L.marker([inst.location.lat, inst.location.lng], {
        icon: window.L.mapbox.marker.icon({
          'marker-color': '#f86767'
        })
      });
      instructionMarker.addTo(map);
    };

    var orderMarkers = [];
    function drawOrderMarkers() {
      for(var i = 0; i < orderMarkers.length; i++) {
        map.removeLayer(orderMarkers[i]);
      }
      orderMarkers = [];
      var orders = getDeliveries();
      for(i = 0; i < orders.length; i++) {
        var orderMarker = window.L.marker([orders[i].location.lat, orders[i].location.lng], {
          icon: window.L.mapbox.marker.icon({
            'marker-color': '#F5AB35'
          })
        });
        orderMarker.addTo(map);
        orderMarker.on('click', itemDetail.bind(this, orders[i]));
        orderMarkers.push(orderMarker);
      }

    }

    var pickupMarkers = [];
    function drawPickupMarkers() {
      for(var i = 0; i < pickupMarkers.length; i++) {
        map.removeLayer(pickupMarkers[i]);
      }
      pickupMarkers = [];
      var pickups = getPickups();
      for(i = 0; i < pickups.length; i++) {
        var pickupMarker = window.L.marker([pickups[i].location.lat, pickups[i].location.lng], {
          icon: window.L.mapbox.marker.icon({
            'marker-color': '#0091ea'
          })
        });
        pickupMarker.addTo(map);
        pickupMarker.on('click', itemDetail.bind(this, pickups[i]));
        pickupMarkers.push(pickupMarker);
      }
    }

    onManifest(() => {
      drawOrderMarkers();
      drawPickupMarkers();
    });

    onNext(() => {
      drawOrderMarkers();
      drawPickupMarkers();
    });


    onLocation((lat, lng) => {
      if(driverMarker === undefined) {
        driverMarker = window.L.marker([lat, lng], {
          icon: window.L.mapbox.marker.icon({
            'marker-color': '#f86767'
          })
        });
        driverMarker.addTo(map);
      }
      else {
        try {
          driverMarker.setLatLng(window.L.latLng(lat, lng));
        }
        catch(e) {
          console.log(e);
        }
      }
    });

    function drawPolyline(lat, lng) {
      var instruction = getCurrent();

      if(instruction !== null) {

      getToken()
        .then((token) => {
          var routerOptions = {
            uri: "http://" + OSRM_IP + ":5000/viaroute?",
            method: 'GET',
            protocol: 'http:',
          };

          var uri = "http://" + OSRM_IP + ":5000/viaroute?";
          uri += 'loc=' + lat + ',' + lng;
          uri += '&loc=' + instruction.location.lat + ',' + instruction.location.lng;
          routerOptions.uri = uri;

          request(routerOptions)
            .then((response) => {
              var data = JSON.parse(response);
              var coords = poly.decode(data.route_geometry).map((point) => {
                return [point[0] / 10, point[1] / 10];
              });

              polyline.setLatLngs([]);
              for(let i = 0; i < coords.length; i++) {
                polyline.addLatLng(window.L.latLng(coords[i][0], coords[i][1]));
              }
            })
            .catch((error) => {
              console.log('Router problem.');
              console.log(error);
            });
        });
      }
      else {
        polyline.setLatLngs([]);
      }
    }
    drawCallback = drawPolyline;
    onLocation(drawPolyline);
    clearCallback = () => { /*polyline.setLatLngs([]);*/ };
  },
  click: function() {
  },
  render: function() {
    return (<div id='mapMount' onClick={this.click}></div>);
  }
});

