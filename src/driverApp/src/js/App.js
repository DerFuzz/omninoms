console.log('APP ENTERED');
//View components...
import request from 'request-promise';
import polyfill from 'babel/polyfill';
import React from 'react';
import Instruction from './Instruction.js';
import History from './History.js';
import Mapbox from './Map.js';
import ItemDetail from './ItemDetail.js';
import LoginScreen from './LoginScreen.js';
import { getToken } from './login.js';
import { SERVER_IP, getHeaders } from './config.js';

//Model components...
import { onInstruction } from './manager.js';

var injectTapEventPlugin = require('react-tap-event-plugin');
injectTapEventPlugin();

var Router = require('react-router');
var Route = Router.Route;
var RouteHandler = Router.RouteHandler;
var Link = Router.Link;
var Navigation = Router.Navigation;

var InstructionWrapper = React.createClass({
  render() {
    return (
      <Instruction
      />
    );
  }
});

var MapboxWrapper = React.createClass({
  render() {
    return (
      <Mapbox
      />
    );
  }
});

onInstruction(() => {
  console.log('Changing window');
  window.location = '#/instruction';
  console.log('Changed!');
});

var App = React.createClass({
  render() {
    return (<RouteHandler/>);
  }
});

var Dash = React.createClass({
    mixins: [Navigation],

    getInitialState() {

      return { online: false, online_str: "..." };
    },

    componentDidMount() {
      function onlineState() {
        getToken()
          .then(function(token) {
            var theOpts = {
              uri: 'https://' + SERVER_IP + '/driver_online/',
                protocol: 'https:',
                method: 'GET',
                headers: getHeaders({
                Authorization: token,
                'Content-Type': 'application/json'
              })
            };
            request(theOpts)
              .then(function(response) {
                var data = JSON.parse(response);
                var message = data.online ? "ON" : "OFF";

                this.setState({ online: data.online, online_str: message });
              }.bind(this))
              .catch(function(error) {
                console.log("Error is: " + error);
                this.setState({ online_str: "ERR" });
                setTimeout(onlineState, 1000);
              }.bind(this));
          }.bind(this));
      }
      setTimeout(onlineState.bind(this), 500);
    },

    routeInstruction: function () {
      window.location = '#/instruction';
    },

    routeHistory: function() {
      window.location = '#/history';
    },

    toggleOnline: function() {
      console.log("toggle clicked");
      getToken()
        .then((token) => {
          console.log(1);
          this.setState({ online_str: '...' });
          console.log(2);

          var onlineOpts = {
            uri: 'https://' + SERVER_IP + '/driver_online/',
              protocol: 'https:',
              method: 'POST',
              headers: getHeaders({
              Authorization: token,
              'Content-Type': 'application/json'
            }),
            body: JSON.stringify({ online: !this.state.online })
          };

          console.log('Updating online status...');
          request(onlineOpts)
            .then((response) => {
              console.log('Posted online status.');
              console.log(response);

              if(this.state.online) {
                this.setState({ online: false, online_str: "OFF" });
              }
              else {
                this.setState({ online: true, online_str: "ON" });
              }

            })
            .catch((error) => {
              console.log("Error posting status.");
              console.log(error);
              this.setState({ online_str: "Err" });
            });
        });
      console.log('waiting on token...');
    },

    render: function() {
      return (
          <div>
                <nav>
                  <ul>
                    <li>
                      <a href='#/instruction' className={'plain bnt'} onClick={this.routeInstruction}>ORDERS</a>
                    </li>
                    <li>
                      <a href='#/history' className={'plain bnt'} onClick={this.routeHistory}>HISTORY</a>
                    </li>
                    <li>
                      <a href='#/home' className={'plain btn'} >MAP</a>
                    </li>
                    <li>
                      <button className={'plain btn'} onClick={this.toggleOnline}>{this.state.online_str}</button>
                    </li>
                  </ul>
                </nav>
                <MapboxWrapper/>
                <RouteHandler/>
          </div>
         );
     }
});

var Home = React.createClass({
  render() {
    return (<div></div>);
  }
});

var routes = (
  <Route handler={App}>
   <Route name='login' path="/" handler={LoginScreen}/>
   <Route handler={Dash}>
     <Route name='home' path="/home" handler={Home}/>
     <Route name='instruction' path="/instruction" handler={InstructionWrapper}/>
     <Route name='history' path="/history" handler={History}/>
     <Route name='itemdetail' path="/itemdetail" handler={ItemDetail}/>
   </Route>
  </Route>
);



Router.run(routes, Router.HashLocation, function(Handler) {
  React.render(<Handler/>, document.getElementById('react-mount'));
});

