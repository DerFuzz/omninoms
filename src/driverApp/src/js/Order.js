import React from 'react';

export default React.createClass({
  render() {
    var meals = [];
    var rows = ['ordrow1', 'ordrow2'];
    var row = 0;
    this.props.order.meals.forEach((meal) => {
      row = (row + 1) % rows.length;
      var combo = "";
      var size = "";

      if(meal.combo != null) {
        combo = meal.combo.name + " ";
      }
      if(meal.size != null) {
        size = " " + meal.size.name;
      }
      meals.push(<div className={rows[row]}><span className={'bold'}>Items:</span>{ size} {meal.items} {combo}<span className={'bold'}>x{meal.quantity}</span></div>);
      if(meal.requests !== "") {
        meals.push(<div className={rows[row]}><span className={'bold'}>Request:</span> {meal.requests}</div>);
      }
    });

    return (
      <div>
      <p>
        Order #{this.props.order.id}
      </p>
      <p>
        Items:
      </p>
      {meals}
      </div>
    );
  }
});
