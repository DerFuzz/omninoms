var Child = React.createClass({
  getInitialState: function() {
    return { foo: false }
  }, 
  click: function(event) {
    console.log('derp');
    this.setState({ foo: !this.state.foo }); 
  },
  render: function() {
    if(!this.state.foo) {
      return (<div onClick={this.click}>HI!</div>)  
    }

    return (<div onClick={this.click}>NOPE!</div>)
  }        
});
