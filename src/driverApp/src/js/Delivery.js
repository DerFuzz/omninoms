import React from 'react';
import Order from './Order.js';

export default React.createClass({
  render() {
    var phone = this.props.delivery.order.owners[0].phone;
    var phone_str = phone.slice(0, 3) + '-' + phone.slice(3, 6) + '-' + phone.slice(6);
    var time = new Date(this.props.delivery.time_completed * 1000);
    var time_str = time.toLocaleTimeString().replace(/:[0-9][0-9] /, "");
    console.log(time);
    return (
      <div>
        <p>
          Delivery #{this.props.delivery.id}
        </p>
        <p>
          <Order order={this.props.delivery.order}/>
        </p>
      <p>
        Customer: {this.props.delivery.order.owners[0].user.first_name} {this.props.delivery.order.owners[0].user.last_name}
      </p>
      <p>
        Phone: {phone_str}
      </p>
      <p>
        Franchise: {this.props.delivery.order.franchise.name}
      </p>
      <p>
        Delivery Address: {this.props.delivery.location.address}
      </p>
      </div>
    );
  }
});
