import React from 'react';
import Delivery from './Delivery.js';
import Pickup from './Pickup.js';

var _itemDetail;

export function itemDetail(item) {
  _itemDetail = item;
  window.location = "#/itemdetail";
}

export default React.createClass({
  getInitialState() {
    return { instruction: _itemDetail };
  },

  componentWillMount() {
    this.setState({ instruction: _itemDetail });
  },

  back() {
    window.history.go(-1);
  },

  render() {
    var detail = "error";
    if(this.state.instruction.type === 'delivery') {
      detail = <Delivery delivery={this.state.instruction}/>;
    }
    else if(this.state.instruction.type === 'pickup') {
      detail = <Pickup pickup={this.state.instruction}/>;
    }
    return (
      <div className={'content item-detail'}>{detail}
        <button onClick={this.back} className={'waves-effect waves-light btn cyan'}>Back</button>
      </div>


    );
  }
});
