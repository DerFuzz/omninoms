import React from 'react';
import { login, loggedIn } from './login.js';
import { loadNotification } from './notification.js';

export default React.createClass({
  getInitialState() {
    return { message: "" };
  },

  submitLogin() {
    console.log(this.state);
    login(this.state.username, this.state.password)
      .then(() => {
        console.log("LOGIN WORKED!!");
        window.location = '#/instruction';
        loadNotification();
      })
      .catch(function(error) {
        console.log("LOGIN ERROR!?!!");
        this.setState({ message: "Could not login. Check username/password." });
      }.bind(this));
  },

  componentWillMount() {
    if(loggedIn()) {
      loadNotification();
      window.location = "#/home";
    }
  },

  usernameChanged(event) {
    this.setState({ username: event.target.value });
  },

  passwordChanged(event) {
    this.setState({ password: event.target.value });
  },

  render() {
    return (<div className={'login-main'}>
              <div className={'login-sub'}>
                <div>{this.state.message}</div>
                <input onChange={this.usernameChanged} type="text"/>
                <input onChange={this.passwordChanged} type="password"/>
                <br/>
                <button onClick={this.submitLogin} className={'waves-effect waves-light btn login-btn'}>Login</button>
              </div>
            </div>
    );
  }
});
