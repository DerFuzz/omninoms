var opts = require('./options.js');
export var OSRM_IP = '52.2.163.64';
export var SERVER_PORT = '8000';
export var auth = new Buffer('test-driver' + ':' + 'merpderp99').toString('base64');
export var LOGIN_URI = 'https://' + opts.DOMAIN + '/rest-auth/login/';
export var SERVER_IP = opts.DOMAIN;
export function getHeaders(headers) {
  var add = opts.HEADERS;

  for(var i = 0; i < add.length; i++) {
    headers[add[i][0]] = add[i][1];
  }
  console.log("headers...");
  console.log(headers);
  return headers;
}
console.log("CONFIG OK");
