import rq from 'request-promise';
import Promise from 'bluebird';
import {LOGIN_URI, getHeaders} from './config.js';

var token;

var username = "";
var password = "";

function getUsername() {
  return username;
}

function getPassword() {
  return password;
}


export function getUserName() {
  return 'test-driver';
}

export function loggedIn() {
  return localStorage.getItem('token') !== null;
}

export function getToken() {
  if(localStorage.getItem('token') !== null) {
    return new Promise((resolve) => { resolve('Token ' + localStorage.getItem('token')); });
  }
  if(token === undefined) {
    var opts = {
      uri: LOGIN_URI,
      protocol: 'https:',
      method: 'POST',
      headers: getHeaders({
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify({
        username: getUsername(),
        password: getPassword()
      })
    };

    return rq(opts)
      .then((rsp) => {
        var data = JSON.parse(rsp);
        token = data.key;
        console.log("YAY I GOTTA TOKEN! " + token);

        localStorage.setItem('token', token);

        return new Promise((resolve) => { resolve('Token ' + token); });
      });
  }
  else {
    return new Promise((resolve) => { resolve('Token ' + token); });
  }
}
console.log("LOGIN OK");

export function login(_username, _password) {
  username = _username;
  password = _password;
  console.log("un: " + username);
  console.log("pw: " + password);

  return getToken();
}



