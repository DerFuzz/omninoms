import {getCurrent, getOrders, nextStep, onInstruction} from './manager.js';
import description from './description.js';
import {triggerClear, triggerDraw} from './Map.js';
import {getLatLng} from './geolocation.js';
import Order from './Order.js';
var React = require('react');


var vibrate = false;
function vibrateLoop() {
  if(vibrate) {
    try {
      navigator.notification.beep(1);
    }
    catch(e) {
      console.log(e);
    }
    navigator.vibrate(500);
    setTimeout(vibrateLoop, 1200);
  }
}

export default React.createClass({
  getInitialState() {
    return { instruction: getCurrent() };
  },
  transitionIn() {
    $(".abs-cent-out").velocity({
        'bottom': '10%'
    }, 400);
  },


  componentDidMount() {
    console.log('MOUNT!');
    if(this.state.instruction != null &&
       this.state.instruction.status === 1) {
      try {
        var notification = new Media('sound/notification.wav', ()=>{});
        notification.play();
      }
      catch(e) {}
      vibrate = true;
      vibrateLoop();
    }

    this.transitionIn();
  },
  componentDidUpdate() { console.log('UPDATE!'); this.transitionIn(); },
  done: function() {
    vibrate = false;
    var that = this;
    $(".abs-cent-out").velocity({
      'bottom': '-200px'
    }, 400, function() {
       var inst = nextStep();
       triggerClear();
       triggerDraw();
       if(inst === null) {
        console.log('ClEAR!!!');
        window.location = '#/home';
       }
       else {
        that.setState({ instruction: inst });
       }

    });
  },

  render: function() {
    var desc;
    if(this.state.instruction !== null) {
      desc = description(this.state.instruction);
    }
    else {
      window.location = '#/home';
      return (<div/>);
    }

    return (
      <div className={'abs-cent-out'}>
        <div className={'abs-cent-in content mCustomScrollbar'}>
          {desc}
          <div className={'center'}>
          <button onClick={this.done} className={'done waves-effect waves-light cyan btn'}>DONE</button>
          </div>
        </div>
      </div>
        );
  }
});
