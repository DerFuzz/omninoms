import React from 'react';

export default React.createClass({
  render() {

    return (
      <div>
        <p>
          Pickup #{this.props.pickup.id}
        </p>
      <p>
        Franchise: {this.props.pickup.restaurant.franchise.name}
      </p>
      <p>
        Address: {this.props.pickup.restaurant.location.address}
      </p>
      </div>
    );
  }
});
