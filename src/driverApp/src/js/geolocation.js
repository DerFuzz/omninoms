import Promise from 'bluebird';
import request from 'request-promise';
import {SERVER_IP, auth, getHeaders} from './config.js';
import {getToken} from './login.js';

var lat = 0.0;
var lng = 0.0;
var locationCallbacks = [];
var lastWatch = 0;

export function onLocation(callback) {
  locationCallbacks.push(callback);
}


function getLatLng() {
  return [lat, lng];
}

function updateServer(latitude, longitude) {
  getToken()
   .then((token) => {
      var loc_options = {
          uri: 'https://' + SERVER_IP + '/driver_location/',
          method: 'POST',
          protocol: 'https:',
          headers: getHeaders({
            Authorization: token,
            'Content-Type': 'application/json'
          })
      };
      var body = JSON.stringify({lat: latitude, lng: longitude});
      loc_options.body = body;

      request(loc_options)
        .then((response) => {
        })
        .catch((error) => {
          console.log('problem');
          console.log(error);
        });
  });
}


export function watchCurrentPosition() {
  return new Promise((resolve, reject) => {
    var options = {
      timeout: 5000,
      maximumAge: 3000,
      enableHighAccuracy: true
    };
    lastWatch = navigator.geolocation.watchPosition(resolve, reject, options);
  });
}

export function getCurrentPosition() {
  return new Promise((resolve, reject) => {
    var options = {
      timeout: 5000,
      maximumAge: 3000,
      enableHighAccuracy: true
    };
    lastWatch = navigator.geolocation.getPosition(resolve, reject, options);
  });
}



function observePosition(position) {
  lat = position.coords.latitude;
  lng = position.coords.longitude;
  updateServer(lat, lng);

  locationCallbacks.forEach((callback) => { callback(lat, lng); });
}

function catchPositionError(error) {
   console.log('Geolocation error.');
   console.log(error);
}

function promiseChain() {
  navigator.geolocation.clearWatch(lastWatch);
  watchCurrentPosition()
    .then(observePosition)
    .catch(catchPositionError);
}

function watchPositionLoop() {
  window.setInterval(promiseChain, 5500);
  //promiseChain();
}

document.addEventListener('deviceready', watchPositionLoop, false);
