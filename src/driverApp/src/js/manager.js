import request from 'request-promise';
import Promise from 'bluebird';
import {SERVER_IP, auth, getHeaders} from './config.js';
import notification from './notification.js';
import {getToken} from './login.js';

/* Manages the state of the driver's
 * instructions.
 */

function check(x, compare) {
  return (e) => { return compare(e, x); };
}

/* The drivers current open instruction set. */
var manifest = [];
/* Completed instructions */
var completed = [];


export function insertions(a, b, compare) {
  /* Accepts two lists and returns the additional
   * elements in b that are not in a. Elements are
   * assumed to be unique and their order is preserved
   * across a and b.
   */
  var j = 0;
  var inserts = [];
  for(var i = 0; i < a.length; i++) {
    while(!compare(a[i], b[j])) {
      inserts.push({ index: j, val: b[j] });
      j++;
    }
    j++;
  }

  while(j < b.length) {
    inserts.push({index: j, val: b[j] });
    j++;
  }

  return inserts;
}

export function intersect(a, b, compare) {
  /* Returns the intersection of a and b.
   */
  var common = [];

  for(let i in a) {
    if(b.findIndex(check(a[i], compare)) !== -1) {
      common.push(a[i]);
    }
  }

  return common;
}

export function map_ids(transform_manifest) {
  return transform_manifest.map((i) => { return i.id; });
}

export function get_prefix(completed_manifest, current_manifest, new_manifest, compare) {
  var instructions = completed_manifest.concat(current_manifest);

  var current = (current_manifest.length === 0)
    ? instructions.slice(-1)[0] : current_manifest[0];

  if(current !== undefined) {
    var index = new_manifest.findIndex((e) => { return compare(e, current); });
    var prefix, tail, completed_tail;

    if(index === -1) {
      prefix = [];
      tail = new_manifest;
      completed_tail = [];
    }
    else {
      prefix = new_manifest.slice(0, index);
      tail = new_manifest.slice(index);
      completed_tail = instructions.slice(-prefix.length - 1, -1);
    }
  }
  else {
    completed_tail = [];
    tail = new_manifest;
  }

  return { prefix: prefix, tail: tail, completed_tail: completed_tail };
}

export function accept_prefix(completed_manifest, current_manifest, new_manifest, compare) {
  /* Returns a boolean indicating whether the
   * prefix of the new manifest can be accepted
   */
  var result = get_prefix(completed_manifest, current_manifest, new_manifest, compare);

  if(intersect(result.prefix, result.completed_tail, compare).length !== result.prefix.length) {
    return false;
  }

  return true;
}

export function filter_deliveries(new_manifest) {
  return new_manifest.filter((i) => { return i.type === 'delivery'; });
}

export function filter_pickups(new_manifest) {
  return new_manifest.filter((i) => { return i.type === 'pickup'; });
}

export function past_pickup(delivery, new_manifest) {
  //Determine if the delivery is past pickup.
  var pickups = filter_pickups(new_manifest);
  var valid_pickups = pickups.filter((pickup) => {
    console.log('Pickup status: ' + pickup.status);
    // status == 3 => in progress
    // status == 4 => done
    if(pickup.status === 3 || pickup.status === 4) {
      console.log('R1');
      return false;
    }
    if(pickup.restaurant.franchise.id !== delivery.order.franchise.id) {
      console.log('R2');
      return false;
    }
    if(pickup.index > delivery.index) {
      console.log('R3');
      return false;
    }

    return true;
  });

  if(valid_pickups.length === 0) {
    return true;
  }

  return false;
}

export function merge(current, next, compare) {
  var result = next.slice(0);
  for(let i = 0; i < current.length; i++) {
    var index = next.findIndex(check(current[i], compare));
    result[index] = current[i];
  }

  return result;
}

export function accept(completed_manifest, current_manifest, new_manifest, compare) {
  /* Takes a potential manifest and returns
   * a boolean indicating whether it should
   * be accepted or not.
   */
  if(current_manifest.length === 0 && completed.length === 0) {
    return true;
  }
  var intersection = intersect(completed_manifest.concat(current_manifest), new_manifest, compare);
  if(intersection.length === 0) {
    return true;
  }

  if(accept_prefix(completed_manifest, current_manifest, new_manifest, compare)) {
    var result = get_prefix(completed_manifest, current_manifest, new_manifest, compare);
    var consider = merge(current_manifest, result.tail, compare);

    var new_instructions = insertions(current_manifest, consider, compare).map((e) => {
      return e.val;
    });
    var deliveries = filter_deliveries(new_instructions);
    var invalid_deliveries = deliveries.filter((delivery) => {
      return past_pickup(delivery, consider);
    });

    if(invalid_deliveries.length === 0) {
      return true;
    }
  }

  return false;
}

function manifest_compare(a, b) {
  return a.id === b.id;
}

var manifestCallbacks = [];
var instructionCallbacks = [];

export function onManifest(callback) {
  manifestCallbacks.push(callback);
}

export function onInstruction(callback) {
  instructionCallbacks.push(callback);
}

var firstRun = true;
export function confirm(unconfirmed) {
  var last;

  for(let i = 0; i < unconfirmed.length; i++) {
    var other = unconfirmed[i];
    if('instructions' in other) {
      other = unconfirmed[i].instructions;
    }
    if(accept(completed, manifest, other, manifest_compare)) {
      last = unconfirmed[i];
    }
  }
  if(last !== undefined) {
    var result = get_prefix(completed, manifest, last.instructions, manifest_compare);
    var consider = merge(manifest, result.tail, manifest_compare);

    setManifest(consider, !firstRun);
    firstRun = false;
  }
  return last;
}

export function getCurrent() {
  if(manifest.length === 0) {
    return null;
  }
  return manifest[0];
};

var onNextCallbacks = [];
export function onNext(callback) {
  onNextCallbacks.push(callback);
}

export function syncManifest() {
  getToken()
  .then((token) => {
    var opts = {
        uri: 'https://' + SERVER_IP + '/manifest/',
        method: 'POST',
        protocol: 'https:',
        body: JSON.stringify({instructions: manifest}),
        headers: getHeaders({
          Authorization: token,
          'Content-Type': 'application/json'
        })
    };
    return request(opts)
      .then((response) => {
        console.log('Manifest update.');
        manifest.forEach((e) => { console.log(e); });
      })
      .catch((error) => {
        console.log('Manifest update error.');
        console.log(error);
      });
  });
}

export function nextStep() {
  console.log('Next step entered...');
  if(manifest.length !== 0) {
    if(manifest[0].status >= 3) {
      manifest[0].orders = getOrders();
      completed.push(manifest[0]);
      manifest[0].status = 4;
      manifest = manifest.slice(1);
      onNextCallbacks.forEach((callback) => { callback(); });
    }
    if(manifest.length !== 0) {
      manifest[0].status++;
    }
  }

  syncManifest();

  return getCurrent();
}

export function setManifest(newInstructions, triggerNext) {
    var last_size = manifest.length;
    manifest = newInstructions;

    for(let i = 0; i < manifest.length; i++) {
      if(manifest[i].status === 0) {
        manifest[i].status = 1;
      }
    }


    manifestCallbacks.forEach((callback) => { callback(manifest); });
    /*
    if(manifest.length > 0 && manifest[0].status <= 1) {
      manifest[0].status = 1;
    }
    */

    if(last_size === 0) {
      instructionCallbacks.forEach((callback) => { callback(); });
    }
    else {
      console.log('Merged manifests...');
    }

    console.log('Manifest set to...');
    manifest.forEach((e) => { console.log(e); });
}


export function initManifest(serverState) {
  if(manifest.length === 0) {
    setManifest(serverState.instructions, !firstRun);
  }
}

var notification_ref = notification(confirm, initManifest);

export function getHistory(callback) {
  if(completed.length !== 0) {
    console.log('Returning cached:: ' + completed);
    callback(completed);
    return;
  }
  getToken()
  .then((token) => {
    var opts = {
        uri: 'https://' + SERVER_IP + '/history/',
        method: 'GET',
        protocol: 'https:',
        headers: getHeaders({
          Authorization: token,
          'Content-Type': 'application/json'
        })
    };
    console.log('Going to fetch history.');
    return request(opts)
      .then((response) => {
        var data = JSON.parse(response);
        console.log('Retrieved History.');
        completed = data;
        callback(data);
      })
      .catch((error) => {
        console.log('Error fetching history.');
        console.log(error);
      });
  });
}

export function getOrders() {
  var current = manifest[0];

  var orders = [];
  try {
    manifest.forEach((instruction) => {
      if(instruction.type === 'delivery') {
        orders.push(instruction.order);
      }
    });

    return orders;
  }
  catch(e) {
    console.log("getOrders problem:");
    console.log(e);
  }
}

export function getDeliveries() {
  var current = manifest[0];

  var orders = [];
  try {
    manifest.forEach((instruction) => {
      if(instruction.type === 'delivery') {
        orders.push(instruction);
      }
    });

    return orders;
  }
  catch(e) {
    console.log("getOrders problem:");
    console.log(e);
  }
}

export function getPickups() {
  var current = manifest[0];

  var orders = [];
  try {
    manifest.forEach((instruction) => {
      if(instruction.type === 'pickup') {
        orders.push(instruction);
      }
    });

    return orders;
  }
  catch(e) {
    console.log("getPickups problem:");
    console.log(e);
  }
}
