var manager = require('../manager.js');
var assert = require('assert');

describe('manager.insertions', () => {
  function compare(a, b) {
    return a === b;
  }
  it('returns equal for empty arrays', () => {
    assert.deepEqual(manager.insertions([], [], compare), []);
  });

  it('returns equal for equal arrays', () => {
    var a = [1, 2, 3];
    var b = [1, 2, 3];
    assert.deepEqual(manager.insertions(a, b, compare), []);
  });

  it('returns differing objects for different arrays', () => {
    var a = [1, 2, 3];
    var b = [0, 1, 2, 4, 3, 5];
    var expected = [
      {
        index: 0,
        val: 0
      },
      {
        index: 3,
        val: 4
      },
      {
        index: 5,
        val: 5
      }
    ];
    assert.deepEqual(manager.insertions(a, b, compare), expected);
  });
});

describe('manager.intersect', () => {
  function compare(a, b) {
    return a === b;
  }
  it('returns empty for empty arrays', () => {
    assert.deepEqual(manager.intersect([], [], compare), []);
    assert.deepEqual(manager.intersect([], [1], compare), []);
    assert.deepEqual(manager.intersect([1], [], compare), []);
  });
  
  it('returns common elements', () => {
    assert.deepEqual(manager.intersect([1, 2, 3], [1, 2, 3], compare), [1, 2, 3]);
    assert.deepEqual(manager.intersect([1, 2, 3], [1, 2, 3, 4], compare), [1, 2, 3]);
    assert.deepEqual(manager.intersect([1, 2, 3, 4], [1, 2, 3], compare), [1, 2, 3]);
    assert.deepEqual(manager.intersect([1, 2, 3, 4, 5], [0, 1, 2, 3], compare), [1, 2, 3]);
  });
});

describe('manager.accept_prefix', () => {
  function compare(a, b) {
    return a === b;
  }
  it('returns true for a matching prefix', () => {
    assert.equal(manager.accept_prefix([1], [2], [2, 3], compare), true);
    assert.equal(manager.accept_prefix([], [2], [2, 3], compare), true);
    assert.equal(manager.accept_prefix([2], [], [2, 3], compare), true);
    assert.equal(manager.accept_prefix([1], [2], [1, 2, 3], compare), true);
  });

  it('returns false for a non-matching prefix', () => {
    assert.equal(manager.accept_prefix([1], [2], [4, 2, 3], compare), false);
    assert.equal(manager.accept_prefix([], [2], [4, 2, 3], compare), false);
    assert.equal(manager.accept_prefix([2], [], [4, 2, 3], compare), false);
  });
});

describe('manager.filter_deliveries', () => {
  it('returns delivery objects from a manifest', () => {
    var manifest = {
      "instructions": [
        {
          "type": "pickup"
        },
        {
          "type": "delivery"
        }
      ]};
    assert.deepEqual(manager.filter_deliveries(manifest.instructions)[0], manifest.instructions[1]);
  });
});

describe('manager.filter_pickups', () => {
  it('returns pickup objects from a manifest', () => {
    var manifest = {
      "instructions": [
        {
          "type": "pickup"
        },
        {
          "type": "delivery"
        }
      ]};
    assert.deepEqual(manager.filter_pickups(manifest.instructions)[0], manifest.instructions[0]);
  });
});

describe('manager.past_pickup', () => {
  it('returns false if the delivery can still be satisfied', () => {
    var manifest = {
      "instructions": [
        {
          "index": 0,
          "status": 2,
          "type": "pickup",
          "restaurant": {
            franchise: { id: 0 }
          }
        },
        {
          "index": 1,
          "type": "delivery",
          "order": {
            franchise: { id: 0 }
          }
        }
      ]};
    assert.equal(manager.past_pickup(manifest.instructions[1], manifest.instructions), false);
  });
  it('returns true if the delivery can not be satisifed', () => {
    var manifest = {
      "instructions": [
        {
          "index": 0,
          "status": 2,
          "type": "pickup",
          "restaurant": {
            "franchise": {"id" :1}
          }
        },
        {
          "index": 1,
          "status": 3,
          "type": "pickup",
          "restaurant": {
            "franchise":  {"id" :0}
          }
        },
        {
          "index": 2,
          "status": 4,
          "type": "pickup",
          "restaurant": {
            "franchise": {"id" : 0}
          }
        },
        {
          "index": 3,
          "type": "delivery",
          "order": {
            franchise: { id: 0 }
          }
        },
        {
          "index": 4,
          "status": 1,
          "type": "pickup",
          "restaurant": {
            "franchise":  {"id" :0}
          }
        }
      ]};
    assert.equal(manager.past_pickup(manifest.instructions[3], manifest.instructions), true);
  });
});

describe('manager.accept', () => {
  function compare(a, b) {
    return a.id === b.id;
  }
  it('returns true when the current & completed are empty', () => {
    var completed = [];
    var current = [];
    var next = [];
    assert.equal(manager.accept([], [], []), true);
  });
  it('returns true when the new manifest has entirely new instructions', () => {
    var completed = [];
    var current = [{
      id: 0
    }];
    var next = [{
      id: 1
    },
    {
      id: 2
    }];
    assert.equal(manager.accept(completed, current, next, compare), true);
  });
  it('returns true when the new manifest is valid', () => {
    var completed = {instructions: []};
    var current = {instructions: [{
      id: 0,
      index: 0,
      type: 'pickup',
      status: 2,
      restaurant: {
        franchise: { id: 0 }
      }
    }]};
    var next = {instructions: [{
      id: 0,
      index: 0,
      type: 'pickup',
      status: 0,
      restaurant: {
        franchise: { id: 0 }
      }},
      {
      index: 1,
      id: 1,
      type: 'delivery',
      status: 0,
      order: {
        franchise: { id: 0 }
      }}]};
    assert.equal(manager.accept(completed.instructions, current.instructions, next.instructions, compare), true);
  });
  it('returns false when the new manifest is invalid', () => {
    var completed = {instructions: []};
    var current = {instructions: [{
      id: 0,
      index: 0,
      type: 'pickup',
      status: 3,
      restaurant: {
        franchise: { id: 1 }
      }
    }]};
    var next = {instructions: [{
      id: 0,
      index: 0,
      type: 'pickup',
      status: 0,
      restaurant: {
        franchise: { id: 0 }
      }},
      {
      id: 1,
      index: 1,
      type: 'delivery',
      status: 0,
      order: {
        franchise: { id: 0 }
      }}]};
    assert.equal(manager.accept(completed.instructions, current.instructions, next.instructions, compare), false);
  });

});
