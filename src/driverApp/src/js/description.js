import React from 'react';
import { getOrders, getDeliveries } from './manager.js';
import Order from './Order.js';
import { itemDetail } from './ItemDetail.js';

export default (instruction) => {
  if(instruction.status === 1) {
    return "New instructions received!";
  }
  if(instruction.status === 2) {
    var msg = 'Drive to ';

    if(instruction.type === 'pickup') {
      msg += instruction.restaurant.franchise.name;

      msg += ' at ' + instruction.location.address;
    }
    if(instruction.type === 'delivery') {
      msg += ' ' + instruction.location.address;
    }

    return msg;
  }
  if(instruction.status === 3) {
   if(instruction.type === 'delivery') {
     var first = instruction.order.owners[0].user.first_name;
     var last = instruction.order.owners[0].user.last_name;
     return <div>
             <p>Deliver and Collect Payment</p>
             <p>Customer: {first} {last} </p>
             <p>Address: {instruction.order.location.address}</p>
             <p>Phone: {instruction.order.owners[0].phone}</p>
             <p>Delivery #{instruction.order.id}</p>
            </div>;
    }
    else if(instruction.type === 'pickup') {
      var desc = [<p>Pickup the following deliveries at {instruction.restaurant.franchise.name}:</p>];

      getDeliveries().forEach((delivery) => {
        desc.push(<button className='waves-effect waves-light btn delivery-button omni-yellow' onClick={itemDetail.bind(this, delivery)}>Delivery #{delivery.id}</button>);
      });
      return desc;
    }
  }
  if(instruction.status === 4) {
      return 'Completed.';
  }

  return "";
};
