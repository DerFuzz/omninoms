import io from 'socket.io-client';
import Promise from 'bluebird';
import {getToken, getUserName, online} from './login.js';
import {SERVER_IP, getHeaders} from './config.js';
import request from 'request-promise';

var confirmCallback;
var initManifest;
export default (_confirmCallback, _initManifest) => {
  confirmCallback = _confirmCallback;
  initManifest = _initManifest;
};

function check_server() {
  getToken()
  .then((token) => {
    var serverStateOpts = {
        uri: 'https://' + SERVER_IP + '/manifest/',
        protocol: 'https:',
        headers: getHeaders({
          Authorization: token,
          'Content-Type': 'application/json'
        })
    };
    var options = {
        uri: 'https://' + SERVER_IP + '/unconfirmed/',
        protocol: 'https:',
        headers: getHeaders({
          Authorization: token,
          'Content-Type': 'application/json'
        })
    };

    var options2 = {
        host: '' + SERVER_IP + '',
        path: '/confirm/',
        method: 'PUT',
        protocol: 'https:',
        headers: getHeaders({
          Authorization: token,
          'Content-Type': 'application/json'
        })
    };

    console.log('Fetching current manifest..');
    request(serverStateOpts)
      .then((response) => {
        console.log('Got manifest from server...');
        console.log(JSON.parse(response));
        initManifest(JSON.parse(response), false);
        console.log('Checking for pending orders.');
        return request(options);
      })
      .then((response) => {
        var data = JSON.parse(response);
        if(data.length === 0) {
          console.log("No pending instructions.");
          return new Promise((resolve, reject) => { resolve('null'); });
        }

        data[0].instructions.forEach((inst) => {
          console.log(inst);
        });

        var conf = confirmCallback(JSON.parse(response));
        var id = (conf === undefined) ? '' : conf.id;

        if(conf !== undefined) {
          console.log(conf.instructions.length + ' instructions.');
        }

        options2.uri = 'https://' + SERVER_IP + '/confirm/' + id;
        console.log('Sending confirmation...');

        return request(options2);
      })
      .then((response) => {
        var data = JSON.parse(response);
        if(data !== null) {
          console.log('Confirmed.');
          console.log(data);
        }
      })
      .catch((error) => {
        console.log('Problem querrying API...');
        console.log(error);
      });
  });
}

export function loadNotification() {
  var socket = io.connect('https://' + SERVER_IP + ':8080');
  socket.on('notification', function() {
    console.log('notification');
    check_server();
  });

  function attemptRegister() {
    getToken()
      .then((token) => {
        console.log('Going to register with token ' + token);
        socket.emit('register', {token: token, user: getUserName()});
      });
  }

  socket.on('connect', function() {
    console.log('Trying to register....');
    attemptRegister();
  });

  socket.on('auth-ok', () => {
    console.log("Authorization OK!");
    check_server();
  });

  socket.on('auth-failed', (msg) => {
    console.log("Could not authenticate");
    console.log(msg);
    setTimeout(() => {
      console.log('Retrying auth...');
      attemptRegister();
    }, 10000);
  });

  socket.on('reconnect', function() {
    console.log('reconnect');
  });

  console.log('notification loaded...');
}
