var fs = require('fs');
var gulp = require('gulp');
var sass = require('gulp-sass');
var del = require('del');
var babel = require('gulp-babel');
var browserify = require('browserify');
var babelify = require('babelify');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var mocha = require('gulp-mocha');
var babel_mocha = require('babel/register');
var replace = require('gulp-replace');
var resolve = require('browser-resolve');

var path = {
  COPY: 'src/www/**',
  SASS: 'src/sass/*.sass',
  JS: 'src/js/**/*.js',
  INDEX: 'src/index.html',
  DEST_SASS: 'www/css',
  DEST_JS: 'www/js',
  DEST_COPY: 'www',
  ENTRY_POINT: 'src/js/App.js',
  OUT: 'build.js'
};


gulp.task('copy', function () {
  gulp.src(path.COPY)
    .pipe(gulp.dest(path.DEST_COPY));
});

gulp.task('compile-sass', function () {
  gulp.src(path.SASS)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(path.DEST_SASS));
});

gulp.task('test', function () {
  return gulp.src('src/**/test*.js', {read: false})
  .pipe(mocha({
    compilers: {
      js: babel_mocha
    }
  }));
});

gulp.task('watch', function () {
  gulp.watch([path.SASS, path.COPY], ['compile-sass', 'copy']);

  var prod = process.argv.slice(2)[0] === '--prod';

  if(!prod) {
    fs.writeFileSync('src/js/options.js',
                     'export var DOMAIN = "starbuck.omninoms.com";' +
                     'export var HEADERS = [["dev-token", "ste3epraspASpeWUnemaCrUnuDathu2E"]];');
  }
  else {
    fs.writeFileSync('src/js/options.js',
                     'export var DOMAIN = "omninoms.com";' +
                     'export var HEADERS = [];');
  }

  var watcher = watchify(browserify({
    entries: [path.ENTRY_POINT],
    transform: [babelify],
    debug: !prod,
    cache: {}, packageCache: {}, fullPaths: true
  }));

  watcher.bundle()
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_JS));
   console.log('Built');


  return watcher.on('update', function () {
    watcher.bundle()
      .pipe(source(path.OUT))
      .pipe(gulp.dest(path.DEST_JS));
     console.log('Updated');
  })
    .bundle()
    .pipe(source(path.OUT))
    .pipe(gulp.dest(path.DEST_JS));
});

gulp.task('clean', function () {
  del(['www']);
});

gulp.task('default', ['copy', 'compile-sass', 'watch']);
