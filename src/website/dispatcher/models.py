from django.conf import settings
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.utils import timezone
import pytz

from dispatcher.logic import (orders as _orders,
                              instruction as _instruction)
from dispatcher.logic import manifest as _manifest
import time


class Base(models.Model):
    """
    Courtesy: http://stackoverflow.com/a/349494
    """
    content_type = models.ForeignKey(ContentType,
                                     editable=False,
                                     null=True)

    def save(self, *args, **kwargs):
        if not self.content_type:
            ct = ContentType.objects.get_for_model(self.__class__)
            self.content_type = ct

        super(Base, self).save(*args, **kwargs)

    def as_leaf_class(self):
        content_type = self.content_type
        model = content_type.model_class()

        if model == Base:
            return self

        return model.objects.get(id=self.id)


class Location(Base):
    lat = models.FloatField(default=0)
    lng = models.FloatField(default=0)
    address = models.CharField(max_length=256)
    city = models.CharField(max_length=256)
    province = models.CharField(max_length=256)
    instructions = models.CharField(max_length=256, default="")

    def __iter__(self):
        return (coord for coord in [self.lat, self.lng])


class Order(Base):
    location = models.ForeignKey('Location')
    franchise = models.ForeignKey('Franchise')
    time_placed = models.DateTimeField(auto_now=True)

    PENDING = 'PENDING'
    DISPATCHED = 'DISPATCHED'
    PROBLEM = 'PROBLEM'
    DONE = 'DONE'
    STATE_CHOICES = (
            (PENDING, 'Pending'),
            (DISPATCHED, 'Dispatched'),
            (PROBLEM, 'Problem'),
            (DONE, 'Done')
    )
    state = models.CharField(max_length=10,
                             choices=STATE_CHOICES,
                             default=PENDING)


class Complete(Base):
    order = models.OneToOneField('Order',
                                 related_name='complete')

    def add(order):
        if len(Complete.objects.filter(order=order)) == 0:
            Complete.objects.create(order=order)

    def remove(order):
        try:
            Complete.objects.get(order=order).delete()
        except ObjectDoesNotExist:
            pass

    def orders():
        return [p.order for p in Complete.objects.all()]

    def take():
        order = Complete.objects.all()[0].order
        Complete.remove(order)

        return order


class Franchise(Base):
    code = models.CharField(max_length=256)
    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)


class Restaurant(Base):
    location = models.ForeignKey('Location')
    franchise = models.ForeignKey('Franchise')
    overhead_pickup = models.FloatField(default=1)
    overhead_order = models.FloatField(default=1)

    estimate_pickup = _orders.Restaurant.estimate_pickup


class Instruction(Base):
    PENDING = 0
    CONFIRMED = 1
    INTRANSIT = 2
    INPROGRESS = 3
    DONE = 4
    STATUS_CHOICES = (
            (PENDING, 'Pending'),
            (CONFIRMED, 'Confirmed'),
            (INTRANSIT, 'In Transit'),
            (INPROGRESS, 'In progress'),
            (DONE, 'Done'),
    )
    status = models.IntegerField(choices=STATUS_CHOICES,
                                 default=PENDING)

    time_completed = models.FloatField(default=0)
    time_estimated = models.FloatField(default=0)
    index = models.IntegerField()

    """
    'Base class' functions
    """
    filter = _instruction.Instruction._filter
    time_estimate = _instruction.Instruction.time_estimate

    class Meta:
        ordering = ['index']


class Delivery(Instruction):
    order = models.ForeignKey('Order')
    location = models.ForeignKey('Location')

    """
    'Base class' functions
    """
    find_pickup = _instruction.Delivery.find_pickup
    time_estimate = _instruction.Delivery.time_estimate


class Pickup(Instruction):
    restaurant = models.ForeignKey('Restaurant')
    location = models.ForeignKey('Location')

    """
    'Base class' functions
    """
    find_deliveries = _instruction.Pickup.find_deliveries
    time_estimate = _instruction.Pickup.time_estimate


class InstructionSet(Base):
    instructions = models.ManyToManyField('Instruction',
                                          related_name='manifest_proxies')

    def all(self):
        return self.instructions.all()

    def set(self, assign):
        self.instructions = assign

    def add(self, instruction):
        self.instructions.add(instruction)

    def remove(self, instruction):
        self.instructions.remove(instruction)


class ConfirmSet(Base):
    manifests = models.ManyToManyField('Manifest')

    def all(self):
        return self.manifests.all()

    def add(self, manifest):
        return self.manifests.add(manifest)

    def remove(self, manifest):
        return self.manifests.remove(manifest)


class Manifest(Base):
    past_pickup = _manifest.Manifest.past_pickup
    __iter__ = _manifest.Manifest.__iter__
    _instructions = None
    instructions = models.ForeignKey('InstructionSet',
                                     null=True)

    def get(self):
        if self.instructions is None:
            self.instructions = InstructionSet.objects.create()

        if self._instructions is None:
            self._instructions = list(self.instructions.all())

        for i in range(0, len(self._instructions)):
            inst = self._instructions[i]
            if type(inst) == Instruction:
                self._instructions[i] = inst.as_leaf_class()

        return self._instructions

    def set(self, instructions):
        for i in range(0, len(instructions)):
            instruction = instructions[i]
            if type(instruction) is _instruction.Pickup:
                instruction = Pickup(location=instruction.location,
                                     restaurant=instruction.restaurant)

            if type(instruction) is _instruction.Delivery:
                instruction = Delivery(order=instruction.order,
                                       location=instruction.location)

            instruction.index = i
            instruction.save()
            instructions[i] = instruction

        self._instructions = instructions
        self.instructions.set(instructions)

    def remove(self, instruction):
        self._instructions = None
        self.instructions.remove(instruction)

    def save(self, *args, **kwargs):
        self.set(self.get())
        super(Manifest, self).save(*args, **kwargs)


class Driver(Base):
    online = models.BooleanField(default=False)
    location = models.ForeignKey('Location')
    instructions = models.OneToOneField('Manifest')
    _confirm_set = models.ForeignKey('ConfirmSet',
                                     null=True)
    user = models.OneToOneField(User, related_name='driver_account')

    completed = models.ForeignKey('InstructionSet',
                                  null=True,
                                  related_name='completed_set')

    def archive(self, instruction):
        if self.completed is None:
            self.completed = InstructionSet.objects.create()

        self.completed.add(instruction)
        if type(instruction) == Delivery:
            Pending.remove(instruction.order)
            Unconfirmed.remove(instruction.order)
            Processing.remove(instruction.order)
            Complete.add(instruction.order)

        instruction.status = 4
        self.instructions.remove(instruction)
        self.instructions.save()
        instruction.time_completed = time.time()
        instruction.save()
        self.completed.save()
        self.save()
        details = ''
        if type(instruction) == Delivery:
            details = "Order #" + str(instruction.order.id) + " complete!"

        if settings.DEBUG is False:
            send_mail('[omninoms.com][complete] {0} #{1}'.format(instruction.__class__.__name__,
                                                                 instruction.id),
                      details,
                      'admin@omninoms.com',
                      ['jon@omninoms.com'])

    def get_unconfirmed(self):
        if self._confirm_set is None:
            self._confirm_set = ConfirmSet.objects.create()

        return self._confirm_set.all()

    def add_unconfirmed(self, manifest):
        self.get_unconfirmed()
        self._confirm_set.add(manifest)

    def remove_unconfirmed(self, manifest):
        try:
            self._confirm_set.manifests.remove(manifest)
            manifest.delete()
        except ObjectDoesNotExist:
            pass

    def confirm(self, manifest_id):
        manifest = Manifest.objects.get(id=manifest_id)
        last = list(self.get_unconfirmed())[-1]

        if manifest != last:
            diff = set(last.get()) - set(manifest.get())

            for instruction in diff:
                if type(diff) == Delivery:
                    Pending.add(instruction.order)
                instruction.delete()

        self._confirm_set.remove(manifest)
        self._confirm_set.all().delete()

        self.instructions = manifest
        self.save()
        manifest.save()


class Pending(Base):
    order = models.OneToOneField('Order',
                                 related_name='pending')

    def add(order):
        if len(Pending.objects.filter(order=order)) == 0:
            Pending.objects.create(order=order)

    def remove(order):
        try:
            Pending.objects.get(order=order).delete()
        except ObjectDoesNotExist:
            pass

    def orders():
        return [p.order for p in Pending.objects.all()]

    def take():
        order = Pending.objects.all()[0].order
        Pending.remove(order)

        return order


class Reschedule(Base):
    order = models.OneToOneField('Order',
                                 related_name='reschedule')

    def add(order):
        if len(Reschedule.objects.filter(order=order)) == 0:
            Reschedule.objects.create(order=order)

    def remove(order):
        try:
            Reschedule.objects.get(order=order).delete()
        except ObjectDoesNotExist:
            pass

    def orders():
        return [p.order for p in Reschedule.objects.all()]

    def take():
        order = Reschedule.objects.all()[0].order
        Reschedule.remove(order)

        return order


class Unconfirmed(Base):
    order = models.OneToOneField('Order',
                                 related_name='unconfirmed')

    def add(order):
        if len(Unconfirmed.objects.filter(order=order)) == 0:
            Unconfirmed.objects.create(order=order)

    def remove(order):
        try:
            Unconfirmed.objects.get(order=order).delete()
        except ObjectDoesNotExist:
            pass

    def orders():
        return [p.order for p in Unconfirmed.objects.all()]

    def take():
        order = Unconfirmed.objects.all()[0].order
        Unconfirmed.remove(order)

        return order


class Processing(Base):
    order = models.OneToOneField('Order',
                                 related_name='processing')

    def add(order):
        if len(Processing.objects.filter(order=order)) == 0:
            Processing.objects.create(order=order)

    def remove(order):
        try:
            Processing.objects.get(order=order).delete()
        except ObjectDoesNotExist:
            pass

    def orders():
        return [p.order for p in Processing.objects.all()]

    def take():
        order = Processing.objects.all()[0].order
        Processing.remove(order)

        return order


class Confirm(Base):
    driver = models.ForeignKey('Driver',
                               related_name='confirmations')
    manifest = models.OneToOneField('Manifest',
                                    related_name='confirmation')
    _accept = models.BooleanField()

    def _add(driver, manifest, accept):
        Confirm.objects.create(driver=driver,
                               manifest=manifest,
                               _accept=accept)

    def accept(driver, manifest):
        Confirm._add(driver, manifest, True)

    def reject(driver, manifest):
        Confirm._add(driver, manifest, False)

    def confirmations():
        return [c for c in Confirm.objects.all()]

    def take():
        confirm = Confirm.objects.all()[0]
        driver = confirm.driver
        accept = confirm._accept
        manifest = confirm.manifest
        confirm.delete()

        return driver, manifest, accept


class Customer(Base):
    user = models.OneToOneField(User,
                                related_name='customer_account')
    orders = models.ManyToManyField('Order',
                                    related_name='owners')
    locations = models.ManyToManyField(Location,
                                       related_name='customers')
    phone = models.CharField(max_length=20)
    created = models.DateTimeField(auto_now=True)


class Hours(Base):
    weekday = models.IntegerField(primary_key=True)
    open_at = models.IntegerField(default=0)
    close_at = models.IntegerField(default=0)


class OpenHours:
    def setDays(day_hours):
        for weekday, open, close in day_hours:
            if weekday not in range(0, 7):
                raise Exception("Not a valid weekday")
            if open not in range(0, 24) or close not in range(0, 24):
                raise Exception("Not valid hours")

            h, _ = Hours.objects.get_or_create(weekday=weekday)
            h.open_at = open
            h.close_at = close

            h.save()

    def getHours():
        timezone.activate(pytz.timezone('Canada/Eastern'))
        t = timezone.localtime(timezone.now())

        return Hours.objects.get(weekday=t.weekday())

    def getHoursString():
        def to_twelve(hour):
            if hour == 0:
                return "Midnight"
            if hour < 12:
                return str(hour) + " am"
            if hour == 12:
                return "Noon"

            return str(hour - 12) + " pm"

        h = OpenHours.getHours()

        return (to_twelve(h.open_at), to_twelve((h.close_at + 1) % 24))

    def open():
        timezone.activate(pytz.timezone('Canada/Eastern'))
        t = timezone.localtime(timezone.now())

        h = OpenHours.getHours()
        if t.hour in range(h.open_at, h.close_at + 1):
            return True

        return False
