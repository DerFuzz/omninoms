# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('dispatcher', '0002_location_instructions'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='created',
            field=models.DateTimeField(auto_now=True, default=datetime.datetime(2015, 7, 22, 22, 49, 19, 338992, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
