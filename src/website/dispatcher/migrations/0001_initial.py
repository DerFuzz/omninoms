# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Base',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Complete',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Confirm',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('_accept', models.BooleanField()),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='ConfirmSet',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('phone', models.CharField(max_length=20)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Driver',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('online', models.BooleanField(default=False)),
                ('_confirm_set', models.ForeignKey(null=True, to='dispatcher.ConfirmSet')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Franchise',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('code', models.CharField(max_length=256)),
                ('name', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Instruction',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('status', models.IntegerField(default=0, choices=[(0, 'Pending'), (1, 'Confirmed'), (2, 'In Transit'), (3, 'In progress'), (4, 'Done')])),
                ('time_completed', models.FloatField(default=0)),
                ('time_estimated', models.FloatField(default=0)),
                ('index', models.IntegerField()),
            ],
            options={
                'ordering': ['index'],
            },
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='InstructionSet',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('lat', models.FloatField(default=0)),
                ('lng', models.FloatField(default=0)),
                ('address', models.CharField(max_length=256)),
                ('city', models.CharField(max_length=256)),
                ('province', models.CharField(max_length=256)),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Manifest',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('instructions', models.ForeignKey(null=True, to='dispatcher.InstructionSet')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('time_placed', models.DateTimeField(auto_now=True)),
                ('state', models.CharField(default='PENDING', choices=[('PENDING', 'Pending'), ('DISPATCHED', 'Dispatched'), ('PROBLEM', 'Problem'), ('DONE', 'Done')], max_length=10)),
                ('franchise', models.ForeignKey(to='dispatcher.Franchise')),
                ('location', models.ForeignKey(to='dispatcher.Location')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Pending',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('order', models.OneToOneField(related_name='pending', to='dispatcher.Order')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Processing',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('order', models.OneToOneField(related_name='processing', to='dispatcher.Order')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Reschedule',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('order', models.OneToOneField(related_name='reschedule', to='dispatcher.Order')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('overhead_pickup', models.FloatField(default=1)),
                ('overhead_order', models.FloatField(default=1)),
                ('franchise', models.ForeignKey(to='dispatcher.Franchise')),
                ('location', models.ForeignKey(to='dispatcher.Location')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.CreateModel(
            name='Unconfirmed',
            fields=[
                ('base_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Base', serialize=False, auto_created=True)),
                ('order', models.OneToOneField(related_name='unconfirmed', to='dispatcher.Order')),
            ],
            bases=('dispatcher.base',),
        ),
        migrations.AddField(
            model_name='base',
            name='content_type',
            field=models.ForeignKey(editable=False, null=True, to='contenttypes.ContentType'),
        ),
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('instruction_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Instruction', serialize=False, auto_created=True)),
                ('location', models.ForeignKey(to='dispatcher.Location')),
                ('order', models.ForeignKey(to='dispatcher.Order')),
            ],
            bases=('dispatcher.instruction',),
        ),
        migrations.CreateModel(
            name='Pickup',
            fields=[
                ('instruction_ptr', models.OneToOneField(primary_key=True, parent_link=True, to='dispatcher.Instruction', serialize=False, auto_created=True)),
                ('location', models.ForeignKey(to='dispatcher.Location')),
                ('restaurant', models.ForeignKey(to='dispatcher.Restaurant')),
            ],
            bases=('dispatcher.instruction',),
        ),
        migrations.AddField(
            model_name='instructionset',
            name='instructions',
            field=models.ManyToManyField(related_name='manifest_proxies', to='dispatcher.Instruction'),
        ),
        migrations.AddField(
            model_name='driver',
            name='completed',
            field=models.ForeignKey(related_name='completed_set', to='dispatcher.InstructionSet', null=True),
        ),
        migrations.AddField(
            model_name='driver',
            name='instructions',
            field=models.OneToOneField(to='dispatcher.Manifest'),
        ),
        migrations.AddField(
            model_name='driver',
            name='location',
            field=models.ForeignKey(to='dispatcher.Location'),
        ),
        migrations.AddField(
            model_name='driver',
            name='user',
            field=models.OneToOneField(related_name='driver_account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='customer',
            name='locations',
            field=models.ManyToManyField(related_name='customers', to='dispatcher.Location'),
        ),
        migrations.AddField(
            model_name='customer',
            name='orders',
            field=models.ManyToManyField(related_name='owners', to='dispatcher.Order'),
        ),
        migrations.AddField(
            model_name='customer',
            name='user',
            field=models.OneToOneField(related_name='customer_account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='confirmset',
            name='manifests',
            field=models.ManyToManyField(to='dispatcher.Manifest'),
        ),
        migrations.AddField(
            model_name='confirm',
            name='driver',
            field=models.ForeignKey(related_name='confirmations', to='dispatcher.Driver'),
        ),
        migrations.AddField(
            model_name='confirm',
            name='manifest',
            field=models.OneToOneField(related_name='confirmation', to='dispatcher.Manifest'),
        ),
        migrations.AddField(
            model_name='complete',
            name='order',
            field=models.OneToOneField(related_name='complete', to='dispatcher.Order'),
        ),
    ]
