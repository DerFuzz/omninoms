# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dispatcher', '0003_customer_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hours',
            fields=[
                ('base_ptr', models.OneToOneField(auto_created=True, to='dispatcher.Base', parent_link=True)),
                ('weekday', models.IntegerField(primary_key=True, serialize=False)),
                ('open_at', models.IntegerField()),
                ('close_at', models.IntegerField()),
            ],
            bases=('dispatcher.base',),
        ),
    ]
