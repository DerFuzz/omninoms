from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.serializers import ValidationError
from dispatcher.models import (Delivery,
                               Location,
                               Order,
                               Pickup,
                               Customer,
                               Restaurant,
                               Franchise,
                               Driver,
                               Manifest)
from customer.models import (Meal,
                             Item,
                             Combo,
                             Size)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('ur', 'name')


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ('lat', 'lng', 'address')


class FranchiseIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Franchise
        fields = ('id', 'name')


class RestaurantSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    franchise = FranchiseIdSerializer()

    class Meta:
        model = Restaurant
        fields = ('franchise', 'location')


class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('name',)


class ComboSerializer(serializers.ModelSerializer):
    class Meta:
        model = Combo
        fields = ('name', )


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Size
        fields = ('name', )


class MealSerializer(serializers.ModelSerializer):
    items = ItemSerializer(read_only=True, many=True)
    combo = ComboSerializer()
    size = SizeSerializer()

    class Meta:
        model = Meal
        fields = ('items', 'combo', 'size', 'quantity', 'requests')


class LimitedUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class CustomerSerializer(serializers.ModelSerializer):
    user = LimitedUserSerializer()
    
    class Meta:
        model = Customer
        fields = ('user', 'phone')


class OrderSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    franchise = FranchiseIdSerializer()
    meals = MealSerializer(read_only=True, many=True)
    owners = CustomerSerializer(read_only=True, many=True)

    class Meta:
        model = Order
        fields = ('id', 'location', 'meals', 'franchise', 'owners')

    """
    def create(self, validated_data):
        serializer = LocationSerializer(data=validated_data['location'])
        if serializer.is_valid():
            l = serializer.save()
        else:
            raise Exception('Malformed request.')

        o = Order.objects.create(location=l,
                                 franchise=validated_data['franchise'])

        return o
    """


class DeliverySerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    order = OrderSerializer()

    class Meta:
        model = Delivery
        fields = ('location', 'order', 'status', 'id', 'index', 'time_completed')


class PickupSerializer(serializers.ModelSerializer):
    location = LocationSerializer()
    restaurant = RestaurantSerializer()

    class Meta:
        model = Pickup
        fields = ('location', 'restaurant', 'status', 'id', 'index', 'time_completed')


class InstructionField(serializers.RelatedField):
    def to_representation(self, value):
        i = value.as_leaf_class()

        if isinstance(i, Delivery):
            obj = DeliverySerializer(instance=i).data
        elif isinstance(i, Pickup):
            obj = PickupSerializer(instance=i).data
        else:
            raise ValidationError('Bad instruction!')

        obj['type'] = i.__class__.__name__.lower()

        return obj

    def to_internal_value(self, data):
        if 'delivery' in data:
            obj = DeliverySerializer(data=data)
        elif 'pickup' in data:
            obj = PickupSerializer(data=data)
        else:
            raise ValidationError('Bad instruction!')

        if obj.is_valid():
            return obj.data
        raise ValidationError('Invalid data!')

    def update(self, instance, validated_data):
        instance.status = validated_data.get('status', instance.status)
        instance.save()


class ManifestSerializer(serializers.ModelSerializer):
    instructions = InstructionField(read_only=True, many=True)

    class Meta:
        model = Manifest
        fields = ('instructions', 'id')




class DriverLocationSerializer(serializers.ModelSerializer):
    location = LocationSerializer()

    class Meta:
        model = Driver
        fields = ('id', 'location', 'address')
