from django.contrib.sites.models import Site
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from dispatcher.models import (Location,
                               Restaurant,
                               Driver,
                               Manifest,
                               Franchise,
                               Customer)
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **kwargs):
        l = Location.objects.create(lat=42.314536, lng=-83.035154, address='50 Someplace Street.')
        

        mcds = Franchise.objects.get(name='Mc Donalds')
        Restaurant.objects.create(location=l,
                                  franchise=mcds,
                                  overhead_pickup=3,
                                  overhead_order=2)

        u = User.objects.create(username='test-driver',
                                password=make_password('merpderp99'))
        l = Location.objects.create(lat=42.29577, lng=-83.02894)
        Driver.objects.create(location=l,
                              instructions=Manifest.objects.create(),
                              user=u)
        u2 = User.objects.create(username='test-customer',
                                 password=make_password('merpderp99'),
				 first_name='Jon',
				 last_name='Donais',
				 email='jon@jadx.ca')
        l = Location.objects.create(
                                     lat=42.30287,
                                     lng=-83.01073,
                                     address='100 Somewhere Ave.',
                                     city='Windsor'
				     )

        c = Customer.objects.create(user=u2, phone='5197840289')
        c.locations.add(l)
        c.save()

        print('Finished initailizing test environment.')
        s = Site.objects.get(pk=1)
        s.domain ='omninoms.com'
        s.name='OmniNoms.com'
        s.save()
