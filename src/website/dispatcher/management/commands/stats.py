from django.core.management.base import BaseCommand
from dispatcher.models import (Pending,
                               Unconfirmed,
                               Processing,
                               Complete,
                               Driver,
                               Order,
                               Delivery)


class Command(BaseCommand):
    help = 'Shows some quick stats on system state used for debugging.'

    def handle(self, *args, **kwars):
        pending = Pending.orders()

        issued_orders = []

        for driver in Driver.objects.all():
            for i in driver.instructions.get():
                if type(i) == Delivery:
                    issued_orders.append(i)

        print("Current Orders     : {0}".format(len(Order.objects.all())))
        print("Pending Orders     : {0}".format(len(pending)))
        print("Unconfirmed Orders : {0}".format(len(Unconfirmed.orders())))
        print("Processing Orders  : {0}",format(len(Processing.orders())))
        print("Complete Orders    : {0}",format(len(Complete.orders())))
        print("Lost Orders        : {0}".format(len(Order.objects.all()) -
                                                len(pending) -
                                                len(Unconfirmed.orders()) -
                                                len(Processing.orders()) -
                                                len(Complete.orders())))
