
from django.contrib.sites.models import Site
from django.contrib.auth.models import User
import googlemaps
import random

from django.contrib.auth.hashers import make_password
from dispatcher.models import (Location,
                               Restaurant,
                               Driver,
                               Manifest,
                               Franchise,
                               Customer)
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Generates random coordinates and reverse geocoes them to a Windsor address.'

    def add_arguments(self, parser):
        parser.add_argument('number', nargs='+', type=int)

    def handle(self, *args, **options):
        gm = googlemaps.Client(key='AIzaSyD5-7y-Us5pGdbMe0puX8xn6G6Kr1r2uj8')

        print('[')
        for i in range(0, options['number'][0]):
            lng = random.uniform(-83.074, -82.91)
            lat = random.uniform(42.279, 42.318)
            if i != 0:
                print(',')
            try:
                result = gm.reverse_geocode((lat, lng))
                print('{"lat": ', lat, ", ",
                       '"lng": ', lng, ", ",
                       '"address": "', result[0]['formatted_address'], '"}')
            except Exception as e:
                pass
        print(']')
