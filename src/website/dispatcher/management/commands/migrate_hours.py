from dispatcher.models import (OpenHours)
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Migrate over to open hours of 4-midnight 7 days a week.'

    def handle(self, *args, **kwargs):
        OpenHours.setDays([(i, 16, 23) for i in range(0, 7)])
