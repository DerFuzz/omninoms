import json
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from dispatcher.serializers import (ManifestSerializer,
                                    OrderSerializer,
                                    DriverLocationSerializer,
                                    PickupSerializer,
                                    DeliverySerializer
                                    )
from dispatcher.models import (Instruction,
                               Pending,
                               Pickup,
                               Delivery,
                               Confirm)
from dispatcher.tasks import dispatcher_manager


def submit_order(customer, order):
    customer.orders.add(order)
    customer.save()
    Pending.add(order)
    # Process order
    dispatcher_manager.delay()


def auth_driver(f, *args):
    def wrapped_args(*args, **kwargs):
        try:
            driver = args[1].user.driver_account
        except ObjectDoesNotExist:
            return Response({'detail': 'Not a driver.'},
                            status=status.HTTP_401_UNAUTHORIZED)
        return f(*args + (driver, ), **kwargs)
    return wrapped_args


def auth_customer(f, *args):
    def wrapped_args(*args, **kwargs):
        try:
            driver = args[1].user.customer_account
        except ObjectDoesNotExist:
            return Response({'detail': 'Not a customer.'},
                            status=status.HTTP_401_UNAUTHORIZED)
        return f(*args + (driver, ), **kwargs)
    return wrapped_args


class UnconfirmedManifests(APIView):
    @auth_driver
    def get(self, request, driver, format=None):
        manifests = driver.get_unconfirmed()
        serializer = ManifestSerializer(manifests, many=True)
        return Response(serializer.data)


class ConfirmManifests(APIView):
    @auth_driver
    def put(self, request, driver, manifest_id='', format=None):
        if 'manifest_id' in request.data:
            manifest_id = request.data['manifest_id']
        accept = None
        rejected = 0
        try:
            for manifest in driver.get_unconfirmed():
                if str(manifest.id) == str(manifest_id):
                    Confirm.accept(driver, manifest)
                    accept = manifest_id
                else:
                    rejected += 1
                    Confirm.reject(driver, manifest)

            # Process confirmation
            dispatcher_manager.delay()
        except:
            return Response({'accept': None, 'rejected': 0})

        """
        WARNING: The accept/reject mechanism here is
        designed rather poorly, it is implicit that if
        the manifest isn't found then it means reject...
        but there could be some weird client/server sync
        issues so it might backfire and cause weird
        behaviour...
        """
        return Response({'accept': accept, 'rejected': rejected})


class Order(APIView):
    @auth_customer
    def post(self, request, customer, format=None):
        data = request.data
        if type(request.data) == str:
            data = json.loads(request.data)
        serializer = OrderSerializer(data=data)

        try:
            if serializer.is_valid():
                order = serializer.save()
            else:
                raise Exception()
        except Exception as e:
            return Response({'detail': 'Malformed payload.'},
                            status=status.HTTP_400_BAD_REQUEST)

        submit_order(customer, order)

        return Response({}, status=status.HTTP_201_CREATED)


class DriverManifest(APIView):
    @auth_driver
    def post(self, request, driver, format=None):
        data = request.data
        if type(data) != dict:
            data = json.loads(request.data)

        instruction_set = set()
        for datum in data['instructions']:
            i_id = datum['id']
            instruction = Instruction.objects.get(id=i_id)

            i_status = datum['status']

            instruction.status = i_status
            instruction.save()
            instruction_set.add(instruction)

        def in_set(instruction):
            for new in instruction_set:
                if new.id == instruction.id:
                    return True

            return False

        for instruction in driver.instructions.get():
            if not in_set(instruction):
                driver.archive(instruction)

        return Response({}, status=status.HTTP_202_ACCEPTED)

    @auth_driver
    def get(self, request, driver, format=None):
        serializer = ManifestSerializer(driver.instructions)
        return Response(serializer.data)


class InstructionHistory(APIView):
    @auth_driver
    def get(self, request, driver, format=None):
        if driver.completed is None:
            return Response({})

        data = []
        for instruction in driver.completed.all():
            instruction = instruction.as_leaf_class()

            if type(instruction) == Pickup:
                serializer = PickupSerializer(instruction)
            if type(instruction) == Delivery:
                serializer = DeliverySerializer(instruction)

            _data = serializer.data
            _data['type'] = instruction.__class__.__name__.lower()
            data.append(_data)

        return Response(data)


class DriverLocation(APIView):
    @auth_driver
    def post(self, request, driver, format=None):
        data = request.data

        if type(data) == str:
            data = json.loads(request.data)

        if 'lat' in data and 'lng' in data:
            driver.location.lat = data['lat']
            driver.location.lng = data['lng']
            driver.location.save()
        else:
            return Response({'detail': 'Malformed payload.'},
                            status=status.HTTP_400_BAD_REQUEST)

        return Response({}, status=status.HTTP_202_ACCEPTED)

    @auth_driver
    def get(self, request, driver, format=None):
        serializer = DriverLocationSerializer(driver)
        return Response(serializer.data)


class DriverOnline(APIView):
    @auth_driver
    def post(self, request, driver, format=None):
        data = request.data

        if type(data) == str:
            data = json.loads(request.data)

        if 'online' in data:
            driver.online = data['online']
            driver.save()
        else:
            return Response({'detail': 'Malformed payload.'},
                            status=status.HTTP_400_BAD_REQUEST)

        return Response({}, status=status.HTTP_202_ACCEPTED)

    @auth_driver
    def get(self, request, driver, format=None):
        return Response({'online': driver.online})


class ConfirmEmail(APIView):
    permission_classes = (IsAuthenticatedOrReadOnly,)

    def get(self, request, format=None, **kwargs):
        return Response({'detail': 'Add e-mail confirmation!'})
