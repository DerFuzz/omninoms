from django.test import TestCase
from django.contrib.auth.models import User
from dispatcher.logic.instruction import RestaurantType
from dispatcher.models import (Restaurant,
                               Franchise,
                               Order,
                               Pending,
                               Driver,
                               Location,
                               Manifest)
from dispatcher.tasks import dispatcher_manager


class TestTasks(TestCase):
    def test_dispatcher_manager(self):
        mcds = Franchise.objects.create(name='Mc Donalds')
        r1 = Restaurant(location=Location.objects.create(lat=42.307825,
                                                         lng=-83.062331),
                        franchise=mcds,
                        overhead_pickup=3,
                        overhead_order=2)
        r1.save()
        o1 = Order(location=Location.objects.create(lat=42.309777,
                                                    lng=-83.065152),
                   franchise=mcds)
        o1.save()
        Pending.add(o1)
        o2 = Order(location=Location.objects.create(lat=42.298184,
                                                    lng=-83.065753),
                   franchise=mcds)
        o2.save()
        Pending.add(o2)

        m = Manifest.objects.create()
        user = User.objects.create(username='test-driver')
        dv = Driver(user=user,
                    location=Location.objects.create(lat=42.300,
                                                     lng=-83.060),
                    instructions=m)
        dv.online = True
        dv.save()

        dispatcher_manager()
        dispatcher_manager()

        dv = Driver.objects.get(id=dv.id)
        self.assertEqual(Pending.orders(), [])
        self.assertGreater(len(dv.get_unconfirmed()), 0)

        dv.online = False
        dv.save()
        Pending.add(o2)

        dispatcher_manager()
        self.assertEqual(len(Pending.orders()), 1)
