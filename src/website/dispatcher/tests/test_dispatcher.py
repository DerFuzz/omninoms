from django.test import TestCase
from django.contrib.auth.models import User

from dispatcher.logic.instruction import RestaurantType, Status
from dispatcher.logic.batcher import Dispatcher
from dispatcher.models import (Driver,
                               Order,
                               Restaurant,
                               Franchise,
                               Location,
                               Manifest)
from dispatcher.logic.instruction import Pickup, Delivery


class DispatcherTest(TestCase):
    def test_assign(self):
        mcds = Franchise.objects.create(name='Mc Donalds')
        r1 = Restaurant(location=Location.objects.create(lat=42.307825,
                                                         lng=-83.062331),
                        franchise=mcds,
                        overhead_pickup=3,
                        overhead_order=2)
        r1.save()
        o1 = Order(location=Location.objects.create(lat=42.309777,
                                                    lng=-83.065152),
                   franchise=mcds)
        o1.save()
        o2 = Order(location=Location.objects.create(lat=42.298184,
                                                    lng=-83.065753),
                   franchise=mcds)
        o2.save()

        m = Manifest.objects.create()
        dv = Driver(user=User.objects.create(username='test-driver'),
                    location=Location.objects.create(lat=42.300,
                                                     lng=-83.060),
                    instructions=m)
        dp = Dispatcher([r1], [(dv, dv.instructions)])

        """
        Vacuous Test
        """
        dv_, tour = dp.assign(o1)
        p1, d1 = tour

        self.assertEqual(type(p1), Pickup)
        self.assertEqual(p1.restaurant.franchise, o1.franchise)
        self.assertEqual(type(d1), Delivery)
        self.assertEqual(d1.order, o1)

        dv.instructions.set(tour)

        p1, d1 = dv.instructions.get()

        """
        Inserting another order from same restaurant.
        """
        dv_, tour = dp.assign(o2)
        p1_, d1_, d2 = tour

        self.assertEqual(p1, p1_)
        self.assertEqual(d1, d1_)
        self.assertEqual(type(d2), Delivery)
        self.assertEqual(d2.order, o2)

        """
        Inserting an order for a past pickup restaurant.
        """
        dv.instructions.get()[1].status = Status.in_progress
        dv.instructions.save()

        self.assertEqual(dp.assign(o2), None)
