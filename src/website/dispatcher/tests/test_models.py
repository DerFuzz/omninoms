from django.test import TestCase
from django.contrib.auth.models import User
from dispatcher.models import (Driver,
                               Delivery,
                               Manifest,
                               Location,
                               Pending,
                               Franchise,
                               Order,
                               Confirm,
                               OpenHours)


class TestModels(TestCase):
    def test_driver_confirm_set(self):
        user = User.objects.create(username='test-driver')
        dv = Driver.objects.create(user=user,
                                   instructions=Manifest.objects.create(),
                                   location=Location.objects.create())

        self.assertEqual(len(dv.get_unconfirmed()), 0)

        new = Manifest.objects.create()
        new2 = Manifest.objects.create()
        new3 = Manifest.objects.create()

        dv.add_unconfirmed(new)
        dv.add_unconfirmed(new2)
        dv.add_unconfirmed(new3)

        self.assertEqual(list(dv.get_unconfirmed()), [new, new2, new3])

        dv.remove_unconfirmed(new2)

        self.assertEqual(list(dv.get_unconfirmed()), [new, new3])

    def test_Pending(self):
        mcds = Franchise.objects.create(name='Mc Donalds')
        o1 = Order.objects.create(location=Location.objects.create(),
                                  franchise=mcds)
        o2 = Order.objects.create(location=Location.objects.create(),
                                  franchise=mcds)
        o3 = Order.objects.create(location=Location.objects.create(),
                                  franchise=mcds)

        Pending.add(o1)
        Pending.add(o2)
        Pending.add(o3)

        """
        Test order of insertion
        """
        self.assertEqual(Pending.orders(), [o1, o2, o3])

        """
        Test remove
        """
        Pending.remove(o2)

        self.assertEqual(Pending.orders(), [o1, o3])

        """
        Test order of reinsertion
        """
        Pending.add(o2)
        self.assertEqual(Pending.orders(), [o1, o3, o2])

    def test_Confirm(self):
        user = User.objects.create(username='test-user')
        dv = Driver.objects.create(user=user,
                                   location=Location.objects.create(),
                                   instructions=Manifest.objects.create())
        m = Manifest.objects.create()
        dv.add_unconfirmed(m)

        Confirm.reject(dv, list(dv.get_unconfirmed())[-1])

        self.assertGreater(len(Confirm.confirmations()), 0)

        driver, manifest, accept = Confirm.take()

        self.assertEqual(list(Confirm.confirmations()), [])
        self.assertEqual((driver, manifest, accept), (dv, m, False))

        Confirm.accept(dv, m)

        driver, manifest, accept = Confirm.take()

        self.assertEqual((driver, manifest, accept), (dv, m, True))

    def test_driver_confirm(self):
        user = User.objects.create(username='test-user')
        dv = Driver.objects.create(user=user,
                                   location=Location.objects.create(),
                                   instructions=Manifest.objects.create())
        m1 = Manifest.objects.create()
        m2 = Manifest.objects.create()
        mcds = Franchise.objects.create(name='Mc Donalds')

        o = Order.objects.create(location=Location.objects.create(),
                                 franchise=mcds)
        d1 = Delivery.objects.create(location=Location.objects.create(),
                                     order=o,
                                     index=0)
        d2 = Delivery.objects.create(location=Location.objects.create(),
                                     order=o,
                                     index=2)
        m1.set([d1])
        m1.save()
        m2.set([d1, d2])
        m2.save()
        dv.add_unconfirmed(m1)
        dv.add_unconfirmed(m2)
        dv.save()

        dv.confirm(m1.id)

        self.assertEqual(len(dv.get_unconfirmed()), 0)
        self.assertEqual(len(Delivery.objects.filter(id=d2.id)), 0)
        self.assertEqual(len(Manifest.objects.filter(id=m2.id)), 0)
        self.assertEqual(dv.instructions, m1)

    def test_OpenHours(self):
        OpenHours.setDays([(i, 0, 0) for i in range(0, 7)])

        self.assertEqual(OpenHours.open(), False)

        OpenHours.setDays([(i, 0, 23) for i in range(0, 7)])
        self.assertEqual(OpenHours.open(), True)
        self.assertRaises(Exception, OpenHours.setDays, [(-1, -1, -1)])
        self.assertRaises(Exception, OpenHours.setDays, [(7, -1, -1)])
        self.assertRaises(Exception, OpenHours.setDays, [(0, -1, -1)])
        self.assertRaises(Exception, OpenHours.setDays, [(0, 24, -1)])
        self.assertRaises(Exception, OpenHours.setDays, [(0, 0, -1)])
        self.assertRaises(Exception, OpenHours.setDays, [(0, 0, 24)])
