import json
from django.contrib.auth.models import User
from rest_framework.test import (APITestCase,
                                 APIRequestFactory,
                                 force_authenticate)
from rest_framework import status

from dispatcher import views
from dispatcher import models


class TestSerializerViews(APITestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        user = User.objects.create(username='test')

        def auth(view, request, user=user):
            force_authenticate(request, user=user)
            response = view.as_view()(request).render()
            data = response.content.decode('utf-8')

            return json.loads(data), response.status_code

        self.auth = auth

        l = models.Location.objects.create()
        mcds = models.Franchise.objects.create(name='Mc Donalds')
        o = models.Order.objects.create(location=l, franchise=mcds)
        d = models.Delivery.objects.create(index=0, order=o, location=l)

        m = models.Manifest.objects.create()
        m.set([d])
        m.save()

    def assertDriverRequired(self, uri):
        request = self.factory.get(uri)
        response, status_code = self.auth(views.UnconfirmedManifests, request)

        self.assertEqual(response, {'detail': 'Not a driver.'})
        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

    def assertCustomerRequired(self, uri):
        request = self.factory.get(uri)
        response, status_code = self.auth(views.UnconfirmedManifests, request)

        self.assertEqual(response, {'detail': 'Not a customer.'})
        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

    def test_unconfirmed_manifests(self):
        request = self.factory.get('/unconfirmed/')
        response, status_code = self.auth(views.UnconfirmedManifests, request)

        self.assertEqual(response, {'detail': 'Not a driver.'})
        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

        user = User.objects.create(username='test-driver')
        location = models.Location.objects.create()
        manifest = models.Manifest.objects.create()
        driver = models.Driver.objects.create(location=location,
                                              instructions=manifest,
                                              user=user)

        m1 = models.Manifest.objects.create()
        mcds = models.Franchise.objects.create(name='Mc Donalds')

        o1 = models.Order.objects.create(location=location, franchise=mcds)
        d1 = models.Delivery.objects.create(location=location,
                                            order=o1,
                                            index=0)
        location2 = models.Location.objects.create()
        d2 = models.Delivery.objects.create(location=location2,
                                            order=o1,
                                            index=1)
        m1.set([d1])
        m2 = models.Manifest.objects.create()
        m2.set([d1, d2])

        driver.add_unconfirmed(m1)
        driver.add_unconfirmed(m2)
        response, _ = self.auth(views.UnconfirmedManifests, request, user=user)

        self.assertEqual(len(response), 2)
        self.assertEqual(len(response[0]['instructions']), 1)
        self.assertEqual(len(response[1]['instructions']), 2)

    def test_confirm_manifests(self):
        request = self.factory.put('/confirm/')
        response, status_code = self.auth(views.ConfirmManifests, request)

        self.assertEqual(response, {'detail': 'Not a driver.'})
        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

        user = User.objects.create(username='test-driver')
        location = models.Location.objects.create()
        manifest = models.Manifest.objects.create()
        driver = models.Driver.objects.create(location=location,
                                              instructions=manifest,
                                              user=user)

        m1 = models.Manifest.objects.create()
        mcds = models.Franchise.objects.create(name='Mc Donalds')

        o1 = models.Order.objects.create(location=location, franchise=mcds)
        d1 = models.Delivery.objects.create(location=location,
                                            order=o1,
                                            index=0)
        location2 = models.Location.objects.create()
        d2 = models.Delivery.objects.create(location=location2,
                                            order=o1,
                                            index=1)
        m1.set([d1])
        m2 = models.Manifest.objects.create()
        m2.set([d1, d2])

        driver.add_unconfirmed(m1)
        driver.add_unconfirmed(m2)

        """
        TODO: Add test environment variable that disables celery tasks during
        unit testing.

        request = self.factory.put('/confirm/', {'manifest_id': m1.id})
        response, _ = self.auth(views.ConfirmManifests, request, user=user)

        self.assertEqual(response['accept'], m1.id)
        self.assertEqual(response['rejected'], 1)

        self.assertEqual(len(models.Confirm.objects.all()), 2)
        """

    def test_put_order(self):
        """
        Must be a customer.
        """
        request = self.factory.put('/order/')
        response, status_code = self.auth(views.Order, request)

        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)
        self.assertEqual(response, {'detail': 'Not a customer.'})

        """
        Empty request.
        """

        user = User.objects.create(username='test-customer')
        customer = models.Customer.objects.create(user=user)

        request = self.factory.put('/order/')
        response, status_code = self.auth(views.Order, request, user=user)

        self.assertEqual(status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response, {'detail': 'Malformed payload.'})

        """
        Malformed request.
        """

        request = self.factory.put('/order/', {'kladjalk': 'ljadlka'})
        response, status_code = self.auth(views.Order, request, user=user)

        self.assertEqual(status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response, {'detail': 'Malformed payload.'})

        """
        Create a new order.
        """

        mcds = models.Franchise.objects.create(name='Mc Donalds')
        request = self.factory.put('/order/', {'location': {'lat': 0.0, 'lng': 0.0}, 'franchise': mcds.id })
        response, status_code = self.auth(views.Order, request, user=user)

        self.assertEqual(status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(customer.orders.all()), 1)

    def test_driver_manifest(self):
        user = User.objects.create(username='test-driver')
        lc = models.Location.objects.create()
        mcds = models.Franchise.objects.create(name='Mc Donalds')

        o1 = models.Order.objects.create(location=lc, franchise=mcds)
        d1 = models.Delivery.objects.create(location=lc,
                                            order=o1,
                                            index=0)
        m1 = models.Manifest.objects.create()
        m1.set([d1])
        dv = models.Driver.objects.create(location=lc,
                                          instructions=m1,
                                          user=user)
        m1.save()
        dv.save()

        self.assertDriverRequired('/manifest/')

        request = self.factory.get('/manifest/')
        response, status_code = self.auth(views.DriverManifest,
                                          request,
                                          user=user)

        self.assertEqual(len(response['instructions']), 1)
        self.assertEqual(response['id'], m1.id)
        self.assertEqual(response['instructions'][0]['id'], d1.id)

        request = self.factory.post('/manifest/',
                                    '{"instructions": [], "id": ' +
                                    str(m1.id) + '}')
        response, status_code = self.auth(views.DriverManifest,
                                          request,
                                          user=user)

        self.assertEqual(response, {})

    def test_driver_location(self):
        request = self.factory.post('/driver_location/')
        response, status_code = self.auth(views.DriverOnline, request)

        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

        user = User.objects.create(username='test-driver')
        lc = models.Location.objects.create()
        m1 = models.Manifest.objects.create()
        dv = models.Driver.objects.create(location=lc,
                                          instructions=m1,
                                          user=user)
        m1.save()
        dv.save()

        self.assertDriverRequired('/driver_location/')

        request = self.factory.post('/driver_location/',
                                    '{"lat": 1.0, "lng": 1.0}')
        response, status_code = self.auth(views.DriverLocation,
                                          request,
                                          user=user)

        self.assertEqual(status_code, status.HTTP_202_ACCEPTED)

        request = self.factory.get('/driver_location/')
        response, status_code = self.auth(views.DriverLocation,
                                          request,
                                          user=user)

        self.assertEqual(status_code, status.HTTP_200_OK)
        self.assertEqual(response['location'], {"lat": 1.0, "lng": 1.0})

    def test_driver_online(self):
        request = self.factory.post('/driver_online/')
        response, status_code = self.auth(views.DriverOnline, request)

        self.assertEqual(status_code, status.HTTP_401_UNAUTHORIZED)

        user = User.objects.create(username='test-driver')
        lc = models.Location.objects.create()
        m1 = models.Manifest.objects.create()
        dv = models.Driver.objects.create(location=lc,
                                          instructions=m1,
                                          user=user)
        m1.save()
        dv.save()

        self.assertDriverRequired('/driver_online/')

        request = self.factory.post('/driver_location/',
                                    '{"online": false}')
        response, status_code = self.auth(views.DriverOnline,
                                          request,
                                          user=user)

        self.assertEqual(status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(dv.online, False)

        request = self.factory.post('/driver_location/',
                                    '{"online": true}')
        response, status_code = self.auth(views.DriverOnline,
                                          request,
                                          user=user)

        self.assertEqual(status_code, status.HTTP_202_ACCEPTED)
        self.assertEqual(dv.online, True)
