from time import sleep
from django.core.cache import cache
from django.core.mail import send_mail
from django.conf import settings
from celery import shared_task
from celery.task.control import inspect
from dispatcher.models import (Driver,
                               Restaurant,
                               Delivery,
                               Pending,
                               Reschedule,
                               Unconfirmed,
                               Processing,
                               Complete,
                               Manifest,
                               Confirm)
from dispatcher.logic.batcher import Dispatcher
from urllib import request
from urllib.error import HTTPError

import traceback



def notify_driver(driver):
    print('Notifying driver {0}...'.format(driver.user.username))
    response = request.urlopen('http://127.0.0.1:3001/notify/' +
                               str(driver.user.username))
    return response.readall()


def process_pending():
    print('Processing pending order...')
    drivers = list(Driver.objects.all())
    dm_set = []

    if len(drivers) == 0:
        print('No Drivers!')
        return 

    for driver in drivers:
        if not driver.online:
            continue
        if len(driver.get_unconfirmed()) == 0:
            dm_set.append((driver, driver.instructions))
        else:
            dm_set.append((driver, list(driver.get_unconfirmed())[-1]))

    restaurants = list(Restaurant.objects.all())

    if len(restaurants) == 0:
        raise Exception('No restaurants!')

    order = Pending.take()

    try:
        val = Dispatcher(restaurants, dm_set).assign(order)
    except Exception as e:
        val = None

        print('DISPACHER ERROR!')
        print(traceback.format_exc())
        print(e)

        if settings.DEBUG == False:
            print("Emailing...")
            if cache.add("error-" + str(order.id), 'true', 300):
                send_mail('[omninoms.com][dispatcher] Error!', 
                          str(traceback.format_exc()) + "\n" + str(e),
                          'admin@omninoms.com', ['jon@omninoms.com'])
            else:
                print("Already emailed!")

    if val is None:
        """
        TODO: Add timeout so the server isn't spinning
        it's wheels.
        """

        print('Could not dispatch order!')
        Pending.add(order)

        if settings.DEBUG == False:
            print("Emailing...")
            if cache.add("dispatch-" + str(order.id), 'true', 300):
                send_mail('[omninoms.com][dispatcher] Alarm!', 
                          str('Could not dispatch order #') + str(order.id),
                          'admin@omninoms.com', ['jon@omninoms.com'])
            else:
                print("Already emailed!")

        return
    else:
        Unconfirmed.add(order)

    driver, instructions = val
    m = Manifest.objects.create()
    m.set(instructions)
    m.save()
    driver.add_unconfirmed(m)
    driver.save()
    try:
        notify_driver(driver)
    except HTTPError as e:
        if e.code == 404:
            """
            TODO: Log Driver not online?
            """
            print('Driver not online.')
        else:
            raise e
    except Exception as e:
        print('Promblem contacting notifcation system!')
        print(e)

    Pending.remove(order)


def process_confirmations():
    driver, manifest, accept = Confirm.take()
    print('Processing confirmation...')
    if accept:
        print('Accepted manifest {0}'.format(manifest.id))
        old = driver.instructions
        driver.instructions = manifest
        old.delete()
        driver.instructions.save()
        driver.save()

        for instruction in driver.instructions.get():
            if type(instruction) == Delivery:
                Pending.remove(instruction.order)
                Unconfirmed.remove(instruction.order)
                Processing.remove(instruction.order)
                Processing.add(instruction.order)

        driver._confirm_set.remove(manifest)
    else:
        for instruction in manifest.get():
            if type(instruction) == Delivery:
                if len(Processing.objects.filter(order=instruction.order)) == 0:
                    Pending.remove(instruction.order)
                    Unconfirmed.remove(instruction.order)
                    Pending.add(instruction.order)

        driver.remove_unconfirmed(manifest)


@shared_task
def dispatcher_manager():
    def aquire_lock():
        print('Got lock!')
        return cache.add('dispatcher-lock', 'true', 60 * 5)

    def release_lock():
        print('Giving up lock!')
        cache.delete('dispatcher-lock')

    """
    Monitors the driver confirmation and pending order queues
    and processess them accordingly.

    Queued driver confirmations should be processed before
    processing pending orders as they update system state
    and can invalidate stale dispatches causing extra work.
    """

    while not aquire_lock():
        print('sleeping...')
        sleep(10)


    try:
        while Confirm.confirmations():
            process_confirmations()

        if Pending.orders():
            process_pending()
    finally:
        release_lock()

    scheduled = 0
    i = inspect().scheduled()
    for host in i:
        scheduled = len(i[host])

    if Pending.orders() and scheduled == 0:
        print('Adding in another worker')
        result = dispatcher_manager.apply_async(countdown=10)
        print('... and moving on!')


    return 'Finished Dispatching!'


@shared_task
def long_task():
    from time import sleep
    sleep(3)
    return 3
