import unittest

from dispatcher.logic.manifest import Manifest
from dispatcher.logic.instruction import Delivery
from dispatcher.logic.instruction import Pickup
from dispatcher.logic.orders import Order
from dispatcher.logic.orders import Restaurant
from dispatcher.logic.instruction import Location
from dispatcher.logic.instruction import RestaurantType
from dispatcher.logic.instruction import Status


class ManifestTests(unittest.TestCase):
    def test_Delivery(self):
        o = Order(Location(0, 0), RestaurantType.mc_donalds, 0)
        d = Delivery(o)

        self.assertEqual(d.status, Status.pending)

    def test_Pickup(self):
        r = Restaurant(Location(0, 0), RestaurantType.mc_donalds, 0, 0)
        p = Pickup(r)

        self.assertEqual(p.status, Status.pending)

    def test_past_pickup(self):
        o = Order(Location(0, 0), RestaurantType.mc_donalds, 0)
        o2 = Order(Location(0, 0), RestaurantType.mc_donalds, 0)

        d = Delivery(o)
        d2 = Delivery(o2)

        d.status = Status.in_transit

        m = Manifest()
        m.get().append(d)

        self.assertEqual(m.past_pickup(d2), True)

        m.get()[0].status = Status.pending
        self.assertEqual(m.past_pickup(d2), False)
        m.get()[0].status = Status.confirmed
        self.assertEqual(m.past_pickup(d2), False)
        m.get()[0].status = Status.done
        self.assertEqual(m.past_pickup(d2), False)

        d3 = Delivery(o)
        d3.status = Status.in_transit
        m.get()[0].status = Status.done
        m.get().append(d3)
        self.assertEqual(m.past_pickup(d2), True)
