import unittest

from dispatcher.logic.orders import Order, Restaurant
from dispatcher.logic.instruction import (Delivery,
                                          Pickup,
                                          RestaurantType,
                                          Status)
from dispatcher.logic.manifest import Manifest


class TestDelivery(unittest.TestCase):
    def test_find_pickup(self):
        rest = Restaurant((0, 0), RestaurantType.mc_donalds, 0, 0)
        ord1 = Order((0, 0), RestaurantType.mc_donalds, 0)
        ord2 = Order((0, 0), RestaurantType.subway, 0)

        m = Manifest()
        d1 = Delivery(ord1)

        """
        Vacuous test case
        """
        self.assertEqual(d1.find_pickup(m.get()), None)

        """
        A valid pickup exists.
        """
        p1 = Pickup(rest)
        m.get().append(p1)
        self.assertEqual(d1.find_pickup(m.get()), 0)

        """
        Pickups exist but for different restaurants.
        """
        d2 = Delivery(ord2)
        self.assertEqual(d2.find_pickup(m.get()), None)

        """
        No valid pickup exists
        """
        m.get()[0].status = Status.in_progress
        self.assertEqual(d1.find_pickup(m.get()), None)

    def test_time_estimate(self):
        d1 = Delivery(Order((0, 0), 0, 0))

        self.assertEqual(d1.time_estimate(), 1.0/60.0)


class TestPickup(unittest.TestCase):
    def test_time_estimate(self):
        p1 = Pickup(Restaurant((0, 0), 0, 3, 2))

        self.assertEqual(p1.time_estimate(2), 7.0/60.0)

    def test_find_deliveries(self):
        p1 = Pickup(Restaurant((0, 0), 0, 3, 2))
        d1 = Delivery(Order((0, 0), 0, 0))
        d2 = Delivery(Order((0, 0), 1, 1))

        self.assertEqual(p1.find_deliveries([p1, d1, d2]), [d1])
