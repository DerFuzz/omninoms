import unittest
from dispatcher.logic.batcher import Dispatcher
from dispatcher.logic.instruction import (RestaurantType,
                                          Pickup,
                                          Status,
                                          Delivery)
from dispatcher.logic.orders import Order, Restaurant, Driver


class DispatcherTest(unittest.TestCase):
    def test_insertions(self):
        rest = Restaurant((0, 0), 'a', 0, 0)
        coords = [Order((42.307825, -83.062331), rest, 0),
                  Order((42.309777, -83.065152), rest, 0),
                  Order((42.298184, -83.065753), rest, 0)]

        d = Dispatcher([], [])
        self.assertEqual(d.insertions([coords[0],
                                       coords[2]],
                                      coords[1]),
                         [([(0.01596, 1), (0.07, 2)], 1),
                          ([(0.06288, 1), (0.06608, 2)], 2)])

        self.assertEqual(d.insertions([], coords[0]), [([(0, 0)], 0)])

        self.assertEqual(d.min_insertion([coords[0],
                                          coords[2]],
                                         coords[1]), (0.09916, coords))

        self.assertEqual(d.min_insertion([], coords[0]), (0, [coords[0]]))

        self.assertEqual(d.min_insertion([coords[0]], coords[1]),
                         (0.01596, coords[:2]))

    def test_insert_pickup(self):
        mcds = RestaurantType.mc_donalds
        rest = Restaurant((42.307825, -83.062331), mcds, 3, 2)
        ord1 = Order((42.309777, -83.065152), mcds, 1)

        """
        Vacuous test

        Return should be a time of 0 and a tour of [rest]
        """
        dv = Driver()
        dp = Dispatcher([rest], [(dv, dv.instructions)])

        self.assertEqual(dv.instructions.get(), [])

        time, tour = dp.insert_pickup(dv.instructions, ord1)
        pickup = tour[0]

        self.assertEqual(type(pickup), Pickup)
        self.assertEqual((time, pickup.restaurant), (0, rest))

        """
        Test with two restaurants.

        Return tour should have 'rest' inserted over 'rest2'
        """
        rest2 = Restaurant((42.33, -83.09), mcds, 3, 2)

        dp = Dispatcher([rest, rest2], [(dv, dv.instructions)])
        dv.instructions.get().append(Delivery(ord1))

        time, tour = dp.insert_pickup(dv.instructions, ord1)
        delivery, pickup = tour

        self.assertEqual(pickup.restaurant, rest)

        """
        Test with two restaurants, but the closest
        is the wrong franchise.
        """
        rest.franchise = RestaurantType.subway

        time, tour = dp.insert_pickup(dv.instructions, ord1)
        delivery, pickup = tour

        self.assertEqual(pickup.restaurant, rest2)

    def test_insert_delivery(self):
        mcds = RestaurantType.mc_donalds
        rest = Restaurant((42.307825, -83.062331), mcds, 3, 2)
        ord1 = Order((42.309777, -83.065152), mcds, 1)
        ord2 = Order((42.298184, -83.065753), mcds, 2)

        d1 = Delivery(ord1)

        """
        Ongoing deliveries for current restaurant pickup
        do not insert.
        """
        dv = Driver()
        d1.status = Status.in_progress
        dv.instructions.get().append(d1)
        dp = Dispatcher([rest], [(dv, dv.instructions)])

        self.assertEqual(dp.insert_delivery(dv.instructions, ord1),
                         None)

        """
        No pickups found, insert a pickup and then the delivery

        1. Empty instructions
        """
        dv.instructions.set([])

        pickup, delivery = dp.insert_delivery(dv.instructions, ord1)
        self.assertEqual((type(pickup), pickup.restaurant.franchise),
                         (Pickup, ord1.franchise))
        self.assertEqual((type(delivery), delivery.order),
                         (Delivery, ord1))
        """
        Valid pickup found, insert delivery.
        """
        instructions = dp.insert_delivery(dv.instructions, ord2)

        self.assertEqual(instructions[-1].order, ord2)

        """
        Past pickup, do not insert delivery.
        """
        instructions[1].status = Status.in_progress
        dv.instructions.set(instructions)
        self.assertEqual(dp.insert_delivery(dv.instructions, ord2), None)

        """
        Delivery from a second restaurant.
        """
        instructions[1].status = Status.pending
        rest2 = Restaurant((42.33, -83.09), RestaurantType.subway, 3, 2)
        ord3 = Order((42.299184, -83.063753), RestaurantType.subway, 3)

        dp = Dispatcher([rest, rest2], [(dv, dv.instructions)])

        dv.instructions.set([Pickup(rest),
                             Delivery(ord1),
                             Delivery(ord2)])

        p1, d1, d2, p2, d3 = dp.insert_delivery(dv.instructions, ord3)
        self.assertEqual((p1.restaurant.franchise, d1.order, d2.order),
                         (mcds, ord1, ord2))
        self.assertEqual((p2.restaurant.franchise, d3.order),
                         (RestaurantType.subway, ord3))

    def test_time_estimates(self):
        mcds = RestaurantType.mc_donalds
        p1 = Pickup(Restaurant((42.307825, -83.062331), mcds, 3, 2))
        d1 = Delivery(Order((42.309777, -83.065152), mcds, 1))
        d2 = Delivery(Order((42.298184, -83.065753), mcds, 2))

        dv = Driver((42.300, -83.060))
        dp = Dispatcher([], [])
        ins = [p1, d1, d2]

        """
        Vacuous Test
        """
        self.assertEqual(dp.time_estimate(dv, []), [])

        """
        Estimate time for some orders.
        """
        self.assertEqual(dp.time_estimate(dv, ins),
                         [(p1, 0.15858666666666665),
                          (d1, 0.19121333333333332),
                          (d2, 0.27788)])

    def test_time_guaruntee(self):
        mcds = RestaurantType.mc_donalds
        p1 = Pickup(Restaurant((42.307825, -83.062331), mcds, 3, 2))
        d1 = Delivery(Order((42.309777, -83.065152), mcds, 1))
        d2 = Delivery(Order((42.298184, -83.065753), mcds, 2))

        dv = Driver((42.300, -83.060))
        dp = Dispatcher([], [])
        ins = [p1, d1, d2]

        """
        Vacuous Test
        """
        self.assertEqual(dp.time_guarantee(dv, []), True)

        """
        Time guaruntee OK
        """
        self.assertEqual(dp.time_guarantee(dv, ins), True)

        """
        Time guaruntee violated
        """
        self.assertEqual(dp.time_guarantee(dv, ins, max_wait=0.25), False)

    def test_assign(self):
        mcds = RestaurantType.mc_donalds
        r1 = Restaurant((42.307825, -83.062331), mcds, 3, 2)
        o1 = Order((42.309777, -83.065152), mcds, 1)
        o2 = Order((42.298184, -83.065753), mcds, 2)

        dv = Driver((42.300, -83.060))
        dp = Dispatcher([r1], [(dv, dv.instructions)])

        """
        Vacuous Test
        """

        dv_, tour = dp.assign(o1)
        p1, d1 = tour

        self.assertEqual(type(p1), Pickup)
        self.assertEqual(p1.restaurant.franchise, o1.franchise)
        self.assertEqual(type(d1), Delivery)
        self.assertEqual(d1.order, o1)

        dv.instructions.set(tour)

        """
        Inserting another order from same restaurant.
        """
        dv_, tour = dp.assign(o2)
        p1_, d1_, d2 = tour

        self.assertEqual(p1, p1_)
        self.assertEqual(d1, d1_)
        self.assertEqual(type(d2), Delivery)
        self.assertEqual(d2.order, o2)

        """
        Inserting an order for a past pickup restaurant.
        """
        dv.instructions.get()[1].status = Status.in_progress

        self.assertEqual(dp.assign(o2), None)

        """
        Inserting an order that violates time guarantee.
        """
        dv.instructions.get()[1].status = Status.pending

        self.assertEqual(dp.assign(o2, max_wait=0.25), None)
