import unittest
import time

from dispatcher.logic.orders import Order, Restaurant
from dispatcher.logic.batcher import NaiveSolution
from dispatcher.logic.router import RouterOSRM, MockRouter
from urllib.error import URLError

"""
TODO: Extend test coverage:

    - greedy_tour
    - time_tour
    - min_node


"""


class TestNaiveSolution(unittest.TestCase):
    def setup_NaiveSolution(self):
        t = time.time()

        orders = [Order((0, 0), 'A', t + 1),
                  Order((1, 1), 'A', t + 2),
                  Order((-0.5, 0), 'A', t + 3)]

        restaurants = [Restaurant((5, 5), "A", 3, 2)]

        drivers = [(-4, -4)]

        return NaiveSolution(orders,
                             restaurants,
                             drivers,
                             router=MockRouter())

    def test_NaiveSolution_oldest_order(self):
        n = self.setup_NaiveSolution()

        self.assertEqual(n.oldest(n.orders), n.orders[0])

    def test_NaiveSolution_closest(self):
        n = self.setup_NaiveSolution()

        try:
            self.assertEqual(n.closest(n.orders[0],
                                       n.restaurants),
                             n.restaurants[0])
        except URLError as e:
            print(e)
            print("Is the router on?")

    def test_NaiveSolution_time_pickup(self):
        r = Restaurant((0, 0), 'A', 5, 2)
        o = Order((0, 0), r, 0)
        o2 = Order((0, 0), r, 1)

        n = NaiveSolution([o, o2], [r], [], router=MockRouter)

        self.assertEqual(n.time_pickup([r, o, o2]), 0.15)


class TestRouter(unittest.TestCase):
    def test_RouterOSRM(self):
        try:
            coords = [(42.307825, -83.062331),
                      (42.309777, -83.065152)]

            ret_val = RouterOSRM().route(coords)
            dist = (ret_val['route_summary']
                           ['total_distance'])

            self.assertEqual(dist, 399)

            self.assertEqual(RouterOSRM().length(coords), 399)
            self.assertEqual(RouterOSRM().length([coords[0]]), 0)
            self.assertEqual(RouterOSRM().length([]), 0)

        except URLError as e:
            print(e)
            print("Is the router on?")

    def test_router_time(self):
        coords = [(42.307825, -83.062331),
                  (42.309777, -83.065152),
                  (42.298184, -83.065753)]

        try:
            self.assertEqual(NaiveSolution([], [], [])
                             .router_time(coords),
                             0.09916)
            self.assertEqual(NaiveSolution([], [], [])
                             .router_time([coords[0], coords[0]]),
                             0.0)

        except URLError as e:
            print(e)
            print("Is the router on?")


if __name__ == "__main__":
    unittest.main()
