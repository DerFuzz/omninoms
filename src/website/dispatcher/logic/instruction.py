"""
Defines driver instructions which the system can issue
during the dispatching process.
"""

from collections import namedtuple


class Status:
    """
    Defines the state an order can be in.

    pending => the instruction has not been
    communicated to the driver, could potentially
    be rejected by the driver due to stale system state.

    confirmed => the driver has acknowledged receipt
    of the instruction and accepted it into their delivery
    manifest. The order has not yet been acted upon.

    inprogress => the instruction is currently being
    carried out and can not be modified.

    done => the instruction has been completed.
    """
    pending = 0
    confirmed = 1
    in_transit = 2
    in_progress = 3
    done = 4


class RestaurantType:
    """
    Defines the different restaurants that can be ordered
    from.
    """
    mc_donalds = 0
    tim_hortons = 1
    subway = 2


Location = namedtuple('Location', 'lat lon')


class InstructionType:
    delivery = 0
    pickup = 1


class Instruction:
    def _filter(self, instructions):
        def condition(i):
            return type(self) == type(i)

        return list(filter(condition, instructions))

    def __init__(self):
        self.status = Status.pending
        self.filter = self._filter
        self.completed = 0
        self.estimated = 0

    def time_estimate(self):
        """
        Returns a time estimate for the non geographic
        portion of the delivery, (ex: getting out of
        the car and delivering food)

        Return time is in hours.
        """
        return 0.0


def filter_type(Class, collection):
    def _filter(item):
        return type(item) == Class 

    return list(filter(_filter, collection))


class Delivery(Instruction):
    def __init__(self, order):
        super().__init__()
        self.order = order
        self.location = order.location

    def find_pickup(self, instructions):
        """
        Tries to find a pickup within the current
        manifest that would satisfy the delivery.

        Return value is index of a valid pickup
        or 'None' if one could not be found.
        """

        def valid_pickup(pickup):
            if type(pickup) != Pickup:
                return False

            in_progress = pickup.status == Status.in_progress
            done = pickup.status == Status.done
            franchise = pickup.restaurant.franchise == self.order.franchise

            return not (in_progress or done) and franchise

        pickups = list(filter(valid_pickup, instructions))

        if pickups == []:
            return None

        return instructions.index(pickups[-1])

    def time_estimate(self):
        """
        Time spent out of the car delivering to
        customer
        """
        return 1.0/60.0


class Pickup(Instruction):
    def __init__(self, restaurant):
        super().__init__()
        self.restaurant = restaurant
        self.location = restaurant.location

    def find_deliveries(self, instructions):
        deliveries = filter_type(Delivery, instructions)

        def same_franchise(delivery):
            return delivery.order.franchise == self.restaurant.franchise

        return list(filter(same_franchise, deliveries))

    def time_estimate(self, orders):
        """
        Time spent in the restaurant buying.
        """
        return (self.restaurant.overhead_pickup +
                self.restaurant.overhead_order * orders) / 60.0
