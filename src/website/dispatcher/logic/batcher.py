"""This module defines batching algorithms selects orders
which should be bundled up into into 'manifests' which
are sent out to drivers.

Batching algorithms accept an order manager as
their first input along with configuration options
and return a batching function which when evaluated
returns a manifest."""

from .router import RouterOSRM
from .orders import Restaurant, Order
from .instruction import Delivery, Pickup
from dispatcher.filters import find_pickup, past_pickup, count_deliveries


"""TODO: Implement a cost funciton that calculates cost for an order
in tems of it's distance to it's the boundary node as well as the restaurant.
Ex:

    B-----------O2
    |          /
    |         /
    |        /
    |       /
    |      /
    |     /
    |    /
    |   /
    O1 /
    | /
    |/
    R

    In this example O1 is closer because
    |(B, O1)| + |(O1, R)| < |(B, 02)| + |(O2, R)|

    ...even though the physical proximity of O2
    is closer to B (ie: |(B, O2)| < |(B, O1)|)
"""


class NaiveSolution:
    """
    Implementation of my first attempt at creating a proper dispatching
    algorithm. Fairly simple uses greedy heuristics to decide how to create
    delivery tours.
    """
    def __init__(self,
                 order_set,
                 restaurant_set,
                 driver_set,
                 router=RouterOSRM()):
        self.orders = order_set
        self.restaurants = restaurant_set
        self.router = router
        self.driver_manifest_set = driver_set

    def router_distance(self, points):
        """
        Returns the routers computed length for the given lat/lon points.
        """
        return self.router.length(points)

    def router_time(self, points, average_speed=25):
        """
        Returns the amount of time (in hours) it takes to walk the tour given
        by points (lat/lon pairs) at an average speed given in km/h.
        """

        dist = self.router.length(points)

        return dist / (average_speed * 1000.0)

    def oldest(self, items):
        """
        Returns the oldest order placed.
        """
        return min(items, key=lambda order: order.time)

    def closest(self, item, items):
        """
        Returns the the single closest element of 'items' to 'item' using
        router_time as the distance measure.
        """
        return min(items, key=lambda i:
                   self.router_time([item.location, i.location]))

    def greedy_tour(self, nodes, start):
        """
        Greedily creates a tour given a set of nodes, and a start element.
        Uses 'closest' as the distance measure.
        """
        tour = [start]
        remaining = list(nodes)
        remaining.remove(start)

        while len(remaining) > 0:
            next_node = self.closest(start, remaining)
            remaining.remove(next_node)
            tour.append(next_node)

        return tour

    def tour_times(self, tour):
        """
        Estimates the time of a tour, either aproximately
        summing between individual nodes to take advantage of cacheing.
        Or more preciesly computing the time for a single tour that
        traverses all the nodes.
        """
        times = []
        for i in range(1, len(tour)):
            time = self.router_time([tour[i - 1].location,
                                     tour[i].location])
            times.append((time, i))

        return times

    def tour_time(self, tour):
        return self.router_time([node.location for node in tour])

    def time_pickup(self, tour):
        """
        Estimates the time 'overhead' of physically stoping and picking up food
        at restaurants.
        """
        restaurants = list(filter(lambda node: type(node) == Restaurant, tour))
        orders = list(filter(lambda node: type(node) == Order, tour))
        overhead = 0

        for restaurant in restaurants:
            num_orders = len(list(filter(lambda order:
                                         order.franchise == restaurant,
                                         orders)))

            overhead += (num_orders *
                         restaurant.overhead_order +
                         restaurant.overhead_pickup)

        return overhead / 60.0

    def min_node(self, fixed_nodes, candidate_nodes):
        def time_to_fixed(candidate):
            return sum(list(map(lambda fixed:
                                self.router_time([candidate.location,
                                                 fixed.location]),
                                list(fixed_nodes))))

        return min(candidate_nodes,
                   key=lambda candidate: time_to_fixed(candidate))

    def insertions(self, tour, node, after_index=0):
        if tour == []:
            return [([(0, 0)], 0)]

        times = []

        for i in range(after_index + 1, len(tour) + 1):
            times.append((self.tour_times(tour[:i] + [node] + tour[i:]), i))

        return times

    def min_insertion(self, tour, node, after_index=0):
        """
        Finds the insertion of node into tour that that minimizes
        total tour cost. (Using time_tour as the cost measure).
        """
        times = self.insertions(tour, node, after_index)

        if times == []:
            return 0, []

        total_times = []

        for sub_times, insert_index in times:
            total_times.append((sum([time for time, pair in sub_times]),
                                insert_index))

        _, i = min(total_times)

        min_tour = tour[:i] + [node] + tour[i:]

        return self.tour_time(min_tour), min_tour

    def batch2(self, max_tour_time):
        order = self.oldest(self.orders)

        restaurant = self.closest(order,
                                  filter(lambda r: r == order.restaurant,
                                         self.restaurants))

        tour = [restaurant, order]

        remaining = list(self.orders)
        remaining.remove(order)

        total_time = 0
        while remaining != []:
            candidate = self.min_node(tour, remaining)
            new_tour = tour

            if candidate.restaurant not in tour:
                restaurant = self.min_node(tour,
                                           filter(lambda r:
                                                  r == candidate.restaurant,
                                                  self.restaurants))

                time, new_tour = self.min_insertion(tour, restaurant)

            r_index = new_tour.index(candidate.restaurant)
            tour_time, new_tour = self.min_insertion(new_tour,
                                                     candidate,
                                                     r_index)

            new_total_time = tour_time + self.time_pickup(new_tour)
            if tour_time + self.time_pickup(new_tour) < max_tour_time:
                tour = new_tour
                total_time = new_total_time

            remaining.remove(candidate)

        return tour, total_time


class Dispatcher(NaiveSolution):
    def __init__(self, restaurant_set, driver_set, router_port=5000):
        super().__init__([], restaurant_set, driver_set, RouterOSRM(router_port))

    def insert_pickup(self, instructions, order, driver, max_wait):
        """
        This will return a copy of the driver's manifest
        with a new pickup instruction inserted for the given
        order.

        This function assumes that the caller has
        already ensured the manifest has no open pickup
        instructions that could satisify the order, ie:
        it will always add a new pickup instruction to
        the tour.
        """
        tour = list(instructions)

        print(self.restaurants[0].franchise.id)
        print(order.franchise.id)

        def franchise(restaurant):
            return restaurant.franchise == order.franchise

        restaurants = list(filter(franchise, self.restaurants))
        print('Restaurants...')
        print(restaurants)

        closest_restaurants = []

        for r in restaurants:
            if self.router_time([r.location, order.location]) < 0.25:
                closest_restaurants.append(r)

        if len(closest_restaurants) == 0:
            print("BATCHER LOGIC ERRROR: Closest Restaurants == 0")
            print("Manual intervention required.")

        restaurant = self.min_node([driver] + tour, closest_restaurants)

        if len(tour) == 0:
            restaurant = self.min_node([driver, order], closest_restaurants)

        print("Selected restaurant: ")
        print(restaurant.id)
        return self.min_insertion(tour, Pickup(restaurant))

    def insert_delivery(self, instructions, order, driver, max_wait):
        """
        Attempts to return a copy of the driver's manifest
        with a delivery instruction inserted for the current
        order.

        The function may return 'None' if the minimal delivery
        insertion causes any other delivery in the tour to take
        become long and violate the time guarantee.

        This function may also insert a pickup instruction to
        satisify the delivery if no valid pickups could be
        found.
        """
        delivery = Delivery(order)

        if past_pickup(delivery, instructions):
            print('THIS IS PAST PICKUP YO!')
            return None
        print('NOT PAST PICKUP!')
        for instruction in instructions:
            try:
                print('id: {0}, type: {2}, status: {1}'.format(instruction.id, instruction.status, type(instruction)))
            except:
                pass

        instructions = list(instructions.get())
        p_i = find_pickup(delivery.order.franchise, instructions)

        if p_i is None:
            time, instructions = self.insert_pickup(instructions, order, driver, max_wait)
            p_i = find_pickup(delivery.order.franchise, instructions)

        time, instructions = self.min_insertion(instructions,
                                                delivery,
                                                after_index=p_i)

        return instructions

    def time_estimate(self, instructions):
        """
        Generates a list of time estimates for each
        instruction in the manifest.
        """
        instructions = list(instructions)
        tour = instructions

        times = []
        for i in range(1, len(tour)):
            points = [instruction.location for instruction in tour]
            time = self.router_time(points[i-1:i+1])

            try:
                orders = tour[i].find_deliveries(tour)
                time += tour[i].time_estimate(len(orders))
            except AttributeError:
                time += tour[i].time_estimate()

            try:
                time += times[-1][1]
            except IndexError:
                pass

            times.append((tour[i], time))

        return times

    def time_guarantee(self, driver, instructions, max_wait=0.5):
        """
        Takes in a series of instructions and ensures that
        the time of any delivery does not exceed the specified
        time guaruntee (default 1/2 an hour).

        Return is boolean, True if the instruction set is ok
        False otherwise.
        """
        instructions = list(instructions)

        times = self.time_estimate(instructions)
        for instruction, time in times:
            if time > max_wait:
                return False

        return True

    def assign(self, order, max_wait=0.5):
        """
        Takes in a new order object and assigns it the manifest
        of a driver in the instruction set.

        Returns the driver & updated manifest if one was found,
        or None if no suitable driver manifest could be found.
        """
        print('Order #{0}'.format(order.id))

        def deliveries(pair):
            driver, instructions = pair
            return count_deliveries(instructions)

        dm_sorted = sorted(self.driver_manifest_set,
                           key=deliveries,
                           reverse=True)

        for driver, instructions in dm_sorted:
            new = self.insert_delivery(instructions, order, driver, max_wait)

            if new is None:
                print('New is none!')
                continue

            if not self.time_guarantee(driver, new, max_wait=max_wait):
                print('Time guarantee violated!')
                continue

            return (driver, new)

        return None
