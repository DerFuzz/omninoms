"""
A delivery manifest is a series of
instructions created by the dispatcher
which tell the driver what to do.

This module contains definitions
for creating and operating on manifests.
"""
from .instruction import Status


class Manifest:
    """
    A manifest is a series of instructions
    that tell a driver what to do.
    """
    def __init__(self):
        self._instructions = []

    def __iter__(self):
        return (instruction for instruction in self.get())

    def get(self):
        return self._instructions

    def set(self, instructions):
        self._instructions = instructions

    def past_pickup(self, delivery):
        """
        This function checks to see if there
        are in progress deliveries of the same
        restaurant type (the driver still has food
        to deliver from that restaurant).

        If the return is true then the delivery can
        not be added to the current manifest.

        This is an 'policy' assumption that telling
        a driver to stop at the same restaurant
        again is too annoying/confusing.
        """
        def delivering(other):
            in_transit = other.status == Status.in_transit
            in_progress = other.status == Status.in_progress

            return (in_transit or in_progress)

        return sum(map(delivering, self.get())) != 0
