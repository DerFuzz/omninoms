import googlemaps

class Geocoder:
    def __init__(self):
        self.gmaps = googlemaps.Client(key='AIzaSyDpvte7SF0MiuetTeYC_LrArSOY64sCKjQ')

    def get_location(self, address):
        location = self.gmaps.geocode(address)[0]['geometry']['location']

        return (location['lng'], location['lat'])
