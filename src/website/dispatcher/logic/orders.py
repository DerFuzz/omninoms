"""This module specifies classes that relate to
customer orders and the management of those objects."""

from .manifest import Manifest


class Driver:
    def __init__(self, location=(0, 0), instructions=None):
        if instructions is None:
            instructions = Manifest()

        self.location = location
        self.instructions = instructions


class Order:
    """
    Defines an order object specifying the customers
    location, the restaurant they want to order from
    and the time the order was placed.
    """
    def __init__(self, location, franchise, time):
        self.location = location
        self.franchise = franchise
        self.time = time

    def __repr__(self):
        return "<Order object: '" + repr(self.franchise) + "'>"

    def __str__(self):
        return ("(" + str(self.franchise) + "), " +
                str(self.location) + ", " +
                str(self.time))


class Restaurant:
    """
    Defines a restaurant object specifying the
    customers location, the restaurant they want to
    order from and the time the order was placed.
    """
    def __repr__(self):
        return "<Restaurant object: '" + self.franchise + "'>"

    def __str__(self):
        return self.franchise + ", " + str(self.location)

    def __init__(self,
                 location,
                 franchise,
                 overhead_pickup,
                 overhead_order):

        self.location = location
        self.franchise = franchise
        self.overhead_pickup = overhead_pickup
        self.overhead_order = overhead_order

    def estimate_pickup(self, orders):
        return self.overhead_pickup + self.overhead_order * len(orders)
