"""This module provides an interface to external routing
APIs (ex: OSRM, Valhalla)."""

from urllib import request
from json import loads
import math

CACHE = {}

SERVER_IP = '52.2.163.64'

def api_call(query):
    """
    Queries an API endpoint for some JSON.
    """
    response = request.urlopen(query)

    return loads(response.readall().decode('utf-8'))


class Router:
    """
    Base class for connecting to a remote routing API.
    """
    def route(self, locations):
        """Takes in a list of lat/lon pairs and returns
        routing information."""
        pass

    def length(self, locations):
        """Takes in a list of lat/lon pairs and returns
        the distance of the route."""


class MockRouter(Router):
    def length(self, locations):
        dist = 0
        for i in range(1, len(locations)):
            dist_x = (locations[i][0] - locations[i - 1][0]) ** 2
            dist_y = (locations[i][1] - locations[i - 1][1]) ** 2
            dist += math.sqrt(dist_x + dist_y)

        return dist


class RouterOSRM(Router):
    """
    Connects to an endpoint running Open Source Routing Machine
    """
    def __init__(self, port=5000):
        self.port = str(port)

    def route(self, locations):
        params = ["loc={0},{1}".format(lat, lon) for lat, lon in locations]
        query = ("http://{0}:".format(SERVER_IP) +
                 self.port +
                 "/viaroute?" +
                 "&".join(params))

        if query not in CACHE:
            CACHE[query] = api_call(query)

        return CACHE[query]

    def length(self, locations):
        if len(locations) < 2:
            return 0.0

        ret_val = self.route(locations)

        return ret_val['route_summary']['total_distance']
