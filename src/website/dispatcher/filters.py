from dispatcher import models
from dispatcher.logic import instruction
from dispatcher.logic.instruction import Status


def count_deliveries(instructions):
    def delivery(instruct):
        type_i = type(instruct)
        if type_i == models.Delivery or type_i == instruction.Delivery:
            return True
        return False

    return sum(map(delivery, instructions))


def find_pickup(franchise, instructions):
    def valid_pickup(pickup):
        type_p = type(pickup)
        if type_p != instruction.Pickup and type_p != models.Pickup:
            return False

        in_progress = pickup.status == Status.in_progress
        done = pickup.status == Status.done
        same_franchise = pickup.restaurant.franchise == franchise

        return not (in_progress or done) and same_franchise

    pickups = list(filter(valid_pickup, instructions))

    if pickups == []:
        return None

    return instructions.index(pickups[-1])


def past_pickup(delivery, instructions):
    """
    This function checks to see if there
    are in progress deliveries of the same
    restaurant type (the driver still has food
    to deliver from that restaurant).

    If the return is true then the delivery can
    not be added to the current manifest.

    This is an 'policy' assumption that telling
    a driver to stop at the same restaurant
    again is too annoying/confusing.
    """
    def delivering(other):
        type_i = type(other)

        if type_i == instruction.Pickup or type_i == models.Pickup:
            in_progress = other.status == Status.in_progress
            done = other.status == Status.done
            if in_progress or done:
                return True
            else:
                return False

        # Otherwise it must be a delivery.
        in_transit = other.status == Status.in_transit
        in_progress = other.status == Status.in_progress
        franchise = other.order.franchise == delivery.order.franchise

        return (in_transit or in_progress) and franchise

    return sum(map(delivering, instructions)) != 0
