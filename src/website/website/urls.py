"""website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import include, url
from django.contrib import admin
#from rest_framework.authtoken import views as auth_views

from dispatcher import views
import customer.views as cviews

urlpatterns = [
#    url(r'^api-token-auth/', auth_views.obtain_auth_token),
    url(r'^$', cviews.test, name='test'),
    url(r'^faq/$', cviews.faq, name='faq'),
    url(r'^signup/(?P<code>.*)$', cviews.signup, name='signup'),
    url(r'^login/$', cviews.login_view, name='login'),
    url(r'^accounts/login/$', cviews.login_view, name='login'),
    url(r'^logout/$', cviews.logout_view, name='logout'),
    url(r'^pay/$', cviews.pay, name='pay'),
    url(r'^mcdonalds/(?P<category>[A-Za-z0-9]*)$',
        cviews.mcdonalds,
        name='mcdonalds'),
    url(r'^menu/(?P<restaurant>[A-Za-z0-9]*)/(?P<category>[A-Za-z0-9]*)/$',
        cviews.menu,
        name='menu'),
    url(r'^tray/', cviews.tray, name='tray'),
    url(r'^checkout/', cviews.checkout, name='checkout'),
    url(r'^add_location/', cviews.add_location, name='add_location'),
    url(r'^rest-auth/registration/account-confirm-email/(?P<key>\w+)/$',
        views.ConfirmEmail.as_view()),
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),
#    url(r'^accounts/', include('allauth.urls')),
    url(r'^unconfirmed/', views.UnconfirmedManifests.as_view()),
    url(r'^confirm/(?P<manifest_id>[0-9]*)',
        views.ConfirmManifests.as_view()),
    url(r'^order/', views.Order.as_view()),
    url(r'^manifest/', views.DriverManifest.as_view()),
    url(r'^driver_location/', views.DriverLocation.as_view()),
    url(r'^driver_online/', views.DriverOnline.as_view()),
    url(r'^history/', views.InstructionHistory.as_view()),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^crash_me/', cviews.crash_me),
    url(r'^contact/', cviews.contact, name='contact')

]
urlpatterns += staticfiles_urlpatterns()
