from dispatcher.models import (Franchise)
from customer.models import (Item, Size, Variant, Option, Combo)
from django.core.management.base import BaseCommand

import csv
import json


class Command(BaseCommand):
    help = 'Initialize Mc Donalds Franchise & Menu Information'

    def handle(self, *args, **kwargs):
        mcds = Franchise.objects.create(code="mcds",
                                        name='Mc Donalds',
                                        description='All the burgers!')

        with open('customer/csv/mc_menu.csv') as f:
            reader = csv.reader(f, quotechar="'")

            header = True
            meal_combos = []
            for row in reader:
                row = list(map(lambda s: s.replace("'", '"'), row))

                name, price, category, combos, sizes, variants, options, url = row

                if header:
                    # Skip header
                    header = False
                    continue

                i = Item.objects.create(franchise=mcds,
                                        name=name,
                                        base_price=float(price),
                                        category=category,
                                        img_url=url)

                meal_combos.append((i, combos))

                print("!")
                print(sizes)
                for name, price_change in json.loads(sizes):
                    Size.objects.create(item=i,
                                        name=name,
                                        price_change=price_change)


                print("!!")
                print(variants)
                for name, price_change in json.loads(variants):
                    Variant.objects.create(item=i,
                                           name=name,
                                           price_change=price_change)


                print('!!!')
                print(options)
                for name, price_change in json.loads(options):
                    Option.objects.create(item=i,
                                          name=name,
                                          price_change=price_change)

            for combos in meal_combos:
                print('!!!!')
                print(combos)
                item, combos = combos
                for combo in json.loads(combos):
                    name, price_change, add_ons = combo

                    c = Combo.objects.create(name=name,
                                             main=item,
                                             price_change=price_change)
                    for add_on_name in add_ons:
                        add_on = Item.objects.get(name=add_on_name)
                        c.add_ons.add(add_on)

                    c.save()


