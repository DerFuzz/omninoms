
from customer.models import (Invite)
from django.core.management.base import BaseCommand

import csv
import json
import random


class Command(BaseCommand):
    help = 'Initialize Mc Donalds Franchise & Menu Information'

    def add_arguments(self, parser):
        parser.add_argument('range', nargs='+', type=int)

    def handle(self, *args, **options):
        for i in range(0, options['range'][0]):
            print('omninoms.com/signup/' + Invite.get_code(i))
