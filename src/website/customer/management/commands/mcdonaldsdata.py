from dispatcher.models import (Franchise)
from customer.models import (Item)
from django.core.management.base import BaseCommand

import csv


class Command(BaseCommand):
    help = 'Initialize Mc Donalds Franchise & Menu Information'

    def handle(self, *args, **kwargs):
        """
        mcds = Franchise.objects.create(name='Mc Donalds',
                                        description='All the burgers!')
        """

        sources = (
            (Item.ENTREE, "customer/csv/mc_sand.csv"),
            (Item.BEVERAGE, "customer/csv/mc_bev.csv"),
            (Item.SIDE, "customer/csv/mc_side.csv"),
            (Item.DESERT, "customer/csv/mc_desert.csv"), 
        )

        data = []
        for category, source in sources:
            with open(source) as f:
                reader = csv.reader(f)

                for row in reader:
                    url = row[0]
                    row = row[1:]
                    if len(row) < 2:
                        row.append('')
                    else:
                        row[1] = row[1][1:]

                    row.append(category)                    
                    row.append([])
                    row.append([])
                    row.append(url)
                    data.append(row)

        with open('customer/csv/mc_menu.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow(['name', 'price', 'category', 'options', 'variants', 'img'])
            for row in data:
                writer.writerow(row)
