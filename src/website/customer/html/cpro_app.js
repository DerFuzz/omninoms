!function(w, d, a) {
    var cQuery, i, k = "", m = Math;
    for (i = 0; 8 > i; i += 1)
        k += String.fromCharCode(m.floor(26 * m.random()) + 97);
    cQuery = w[k] = {}, cQuery.w = w, cQuery.d = d, cQuery.a = a, cQuery.log = function() {
        return cQuery.w.console && "production" !== cQuery.a.cpro_env && cQuery.w.console.log && Function.prototype.apply.call(cQuery.w.console.log, cQuery.w.console, arguments)
    };
    var _paq = cQuery.w._paq || (cQuery.w._paq = []), _gaq = cQuery.w._gaq || (cQuery.w._gaq = []);
    cQuery.w.execPiwikSnippet = function(e) {
        if (cQuery.log("execPiwikSnippet callback no nie"), !cQuery.w.pa_installed) {
            var r = r || [];
            !function() {
                var t = ("https:" == cQuery.d.location.protocol ? "https://" : "http://") + cQuery.a.piwik_host + "/";
                r.push(["setSiteId", e.piwik_id]), r.push(["setTrackerUrl", t + "piwik.php"]), r.push(["trackPageView"]), r.push(["enableLinkTracking"]);
                var a = document, s = a.createElement("script"), c = a.getElementsByTagName("script")[0];
                s.type = "text/javascript", s.defer=!0, s.async=!0, s.src = t + "piwik.js", c.parentNode.insertBefore(s, c)
            }(), cQuery.w.pa_installed=!0
        }
    }, embedPiwikUniversalSnippet = function(e) {
        function r(e) {
            for (var r = 1; r < arguments.length; r++)
                e = e.replace(/%s/, arguments[r]);
            return e
        }
        script = cQuery.d.createElement("script"), script.text = r('var _paq = _paq || [];\n(function(){ var u=(("https:" == document.location.protocol) ? "https://%s/" : "http://%s/");\n_paq.push(["setSiteId", %s]);\n_paq.push(["setTrackerUrl", u+"piwik.php"]);\nvar d=document, g=d.createElement("script"), s=d.getElementsByTagName("script")[0]; g.type="text/javascript"; g.defer=true; g.async=true; g.src=u+"piwik.js";\ns.parentNode.insertBefore(g,s); })();', cQuery.a.piwik_host, cQuery.a.piwik_host, e.piwik_id), m = cQuery.d.getElementsByTagName("script")[0], m.parentNode.insertBefore(script, m)
    }, "undefined" == typeof cQuery.f && (cQuery.f = function() {
        var CproApp = {
            api: ("https:" == cQuery.d.location.protocol ? "https://" : "http://") + cQuery.a.server + "/",
            executePiwikFinalTrackers: function() {
                _paq.push(["trackPageView"]), _paq.push(["enableLinkTracking"])
            },
            execPiwikSnippet: function(e) {
                this.pa = e.piwik_id, this.pa&&!this.pa_installed && (u = ("https:" == cQuery.d.location.protocol ? "https://" : "http://") + cQuery.a.piwik_host + "/"), _paq.push(["setTrackerUrl", u + "piwik.php"]), _paq.push(["setSiteId", "" + this.pa]), a = document.createElement("script"), a.type = "text/javascript", a.async=!0, a.src = u + "piwik.js", c = document.getElementsByTagName("script")[0], c.parentNode.insertBefore(a, c), this.pa_installed=!0
            },
            embedGASnippetNew: function() {
                !function(e, r, t, a, s, c, n) {
                    e.GoogleAnalyticsObject = s, e[s] = e[s] || function() {
                        (e[s].q = e[s].q || []).push(arguments)
                    }, e[s].l = 1 * new Date, c = r.createElement(t), n = r.getElementsByTagName(t)[0], c.async = 1, c.src = a, n.parentNode.insertBefore(c, n)
                }(window, document, "script", "//www.google-analytics.com/analytics.js", "ga"), ga("create", cQuery.a.ga_new), ga("require", "ecommerce", "ecommerce.js"), ga("send", "pageview")
            },
            embedGARemarketingSnippet: function() {
                script = cQuery.d.createElement("script"), script.text = 'var _gaq = _gaq || [];_gaq.push(["_setAccount","UA-45431605-1"]);_gaq.push(["_setAllowLinker",true]);_gaq.push(["_setDomainName","none"]);_gaq.push(["_trackPageview"]);\n(function() {\nvar ga = document.createElement("script"); ga.type = "text/javascript"; ga.async = true;\nga.src = "https://stats.g.doubleclick.net/dc.js";\nvar s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ga, s);\n})();', m = cQuery.d.getElementsByTagName("script")[0], m.parentNode.insertBefore(script, m)
            },
            rand: function() {
                return Math.floor(1e4 + 89999 * Math.random())
            },
            set_cookie: function(e, r) {
                var t = new Date;
                t.setTime(t.getTime()), document.cookie = e + "=" + escape(r) + ";expires=" + new Date(t.getTime() + 26784e5).toGMTString()
            },
            get_cookie: function(e) {
                for (var e = e + "=", r = document.cookie.split(";"), t = 0; t < r.length; t++) {
                    for (var a = r[t]; " " == a.charAt(0);)
                        a = a.substring(1, a.length);
                    if (0 == a.indexOf(e))
                        return a.substring(e.length, a.length)
                }
                return null
            },
            ajax_js: function(e) {
                if (e && "null" != e && "/null" != e) {
                    var r = document.getElementsByTagName("head")[0], t = document.createElement("script");
                    t.language = "javascript", t.src = e, t.async=!0, t.defer = "defer", r.appendChild(t)
                }
            },
            prepare_url: function(e, r, t) {
                - 1 == e.search(/http/)&&-1 == e.search(/https/) && (e = this.api + e);
                var a, s = e;
                if (r || (r = {}), t)
                    for (a in t)
                        r[a] = encodeURIComponent(t[a]);
                r.rand = this.rand(), (t = this.get_cookie("convpro.com")) && (r.cookie = t), r.referer = encodeURIComponent(document.referrer), t = [];
                for (a in r)
                    t.push(a + "=" + r[a]);
                return (r = t.join("&")) && ( - 1 == e.search(/http/) && (e = this.api + e), s =- 1 == e.search(/\?/) ? s + ("?" + r) : e.search(/\?/) == e.length - 1 || e.search(/\&/) == e.length - 1 ? s + r : s + ("&" + r)), s
            },
            setupCProWithGAUniversalTrackables: function() {
                var e, r, t, a;
                for (r = cQuery.w.cboost || [], t = {
                    "ecommerce:addTransaction": function(e) {
                        _paq.push(["trackEcommerceOrder", e.id, e.revenue, e.revenue, e.tax, e.shipping, !1])
                    },
                    "ecommerce:addItem": function(e) {
                        _paq.push(["addEcommerceItem", e.sku, e.name, e.category, e.price, e.quantity])
                    },
                    "ecommerce:send": function() {
                        cQuery.log("send method called for google analytics ecommerce tracker")
                    }
                }, cQuery.w.cboost = {
                    push: function(e) {
                        return "[object Array]" === Object.prototype.toString.call(e) && e[0] && t.hasOwnProperty(e[0]) ? t[e[0]].apply(null, e.slice(1)) : !1
                    }
                }, e = 0, a = []; e < r.length;)
                    cQuery.w.cboost.push(r[e]), a.push(e++)
            },
            setupCProWithGAClassicTrackables: function() {
                for (var e = _paq || [], r = 0; r < e.length; r++)
                    _paq.push(e[r])
            },
            getTrackables: function() {
                cQuery.log("getting trackables");
                for (var result = [], scripts = cQuery.d.getElementsByTagName("script"), scraped_orders = [], i = 0; i < scripts.length; i++) {
                    for (var text = scripts[i].innerHTML, gaq_regex = /_gaq.push\(([^)]*)\)/g, match, res = []; match = gaq_regex.exec(text);)
                        res.push(match[1]);
                    scraped_orders.push(res)
                }
                scraped_orders = [].concat.apply([], scraped_orders), cQuery.log("scraped orders are ", scraped_orders);
                for (var parsed_orders = [], i = 0; i < scraped_orders.length; i++) {
                    var res;
                    try {
                        res = eval(scraped_orders[i])
                    } catch (e) {
                        res = null
                    }
                    res && parsed_orders.push(eval(scraped_orders[i]))
                }
                var supported_commands = {
                    _addTrans: 1,
                    _addItem: 1,
                    _trackTrans: 1
                };
                cQuery.log("parsed orders are ", parsed_orders);
                for (var i = 0; i < parsed_orders.length; i++)
                    supported_commands[parsed_orders[i][0]] && result.push(parsed_orders[i]);
                cQuery.w._cpro = cQuery.w._cpro || [];
                for (var i = 0; i < result.length; i++)
                    cQuery.w._cpro.push(result[i])
            },
            getUniversalTrackables: function() {
                cQuery.log("getting trackables");
                for (var result = [], scripts = cQuery.d.getElementsByTagName("script"), scraped_orders = [], i = 0; i < scripts.length; i++) {
                    for (var text = scripts[i].innerHTML, gaq_regex = /ga\(([^)]*)\)/g, match, res = []; match = gaq_regex.exec(text);)
                        res.push(match[1]);
                    scraped_orders.push(res)
                }
                scraped_orders = [].concat.apply([], scraped_orders), cQuery.log("scraped orders are ", scraped_orders);
                for (var parsed_orders = [], i = 0; i < scraped_orders.length; i++) {
                    var res;
                    try {
                        res = eval(scraped_orders[i])
                    } catch (e) {
                        res = null
                    }
                    res && parsed_orders.push(eval(scraped_orders[i]))
                }
                var supported_commands = {
                    _addTrans: 1,
                    _addItem: 1,
                    _trackTrans: 1
                };
                cQuery.log("parsed orders are ", parsed_orders);
                for (var i = 0; i < parsed_orders.length; i++)
                    supported_commands[parsed_orders[i][0]] && result.push(parsed_orders[i]);
                cQuery.w._cpro = cQuery.w._cpro || [];
                for (var i = 0; i < result.length; i++)
                    cQuery.w._cpro.push(result[i])
            },
            init: function() {
                cQuery.log("cpro initialized!"), "undefined" == typeof cQuery.w.name && (cQuery.w.name = ""), this.getTrackables(), this.setupCProWithGAClassicTrackables(), this.executePiwikFinalTrackers()
            }
        };
        return CproApp
    }(), cQuery.w.setTimeout(function() {
        cQuery.f.init()
    }, 100))
}(window, document, {
    server: "convpro.com",
    cpro_env: "production",
    piwik_host: "piwik-convpro.cloudapp.net/piwik",
    ga_host: ".google-analytics.com/ga.js",
    ga: "UA-45431605-1",
    ga_new: "UA-45460714-1"
});

