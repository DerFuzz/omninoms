#!/bin/bash

grep -e "<figcaption>.*</figcaption>" $1 | \
sed -e "s/<img[^>]*src=\"//" | \
sed -e "s/<a[^>]*>//" | \
sed -e "s/<figure>//" | \
sed -e "s/<figcaption>//" | \
sed -e "s/<br>/,/" | \
sed -e "s/<\/figcaption><\/figure><\/a>//" | \
sed -e "s/CA[^0-9]//" | \
sed -e "s/<p>//" | \
sed -e "s/<\/p>//" | \
sed -e "s/&amp//" | \
sed -e "s/^ *//" | \
sed -e "s/\">/,/" 
