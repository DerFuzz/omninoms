# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dispatcher', '0003_customer_created'),
    ]

    operations = [
        migrations.CreateModel(
            name='Combo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('price_change', models.FloatField()),
            ],
        ),
        migrations.CreateModel(
            name='Invite',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('email', models.CharField(max_length=256)),
                ('code', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('description', models.CharField(max_length=512)),
                ('base_price', models.FloatField()),
                ('img_url', models.CharField(max_length=256)),
                ('category', models.IntegerField(choices=[(0, 'Entree'), (1, 'Beverage'), (2, 'Side'), (3, 'Desert')])),
                ('franchise', models.ForeignKey(to='dispatcher.Franchise', related_name='menu')),
            ],
        ),
        migrations.CreateModel(
            name='Meal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('quantity', models.IntegerField(default=1)),
                ('requests', models.CharField(max_length=512)),
                ('description', models.CharField(max_length=512)),
            ],
        ),
        migrations.CreateModel(
            name='Option',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('price_change', models.FloatField()),
                ('name', models.CharField(max_length=256)),
                ('selected', models.BooleanField(default=False)),
                ('item', models.ForeignKey(to='customer.Item', related_name='options')),
            ],
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('price_change', models.FloatField()),
                ('item', models.ForeignKey(to='customer.Item', related_name='sizes')),
            ],
        ),
        migrations.CreateModel(
            name='Variant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('price_change', models.FloatField()),
                ('item', models.ForeignKey(to='customer.Item', related_name='variants')),
            ],
        ),
        migrations.CreateModel(
            name='Special',
            fields=[
                ('meal_ptr', models.OneToOneField(parent_link=True, auto_created=True, to='customer.Meal', serialize=False, primary_key=True)),
                ('price', models.FloatField()),
                ('restaurant', models.ForeignKey(to='dispatcher.Franchise', related_name='specials')),
            ],
            bases=('customer.meal',),
        ),
        migrations.AddField(
            model_name='meal',
            name='items',
            field=models.ManyToManyField(to='customer.Item', related_name='meals'),
        ),
        migrations.AddField(
            model_name='meal',
            name='order',
            field=models.ForeignKey(to='dispatcher.Order', related_name='meals'),
        ),
        migrations.AddField(
            model_name='combo',
            name='add_ons',
            field=models.ManyToManyField(to='customer.Item', related_name='combo_add_ons'),
        ),
        migrations.AddField(
            model_name='combo',
            name='main',
            field=models.ForeignKey(to='customer.Item', related_name='combos'),
        ),
    ]
