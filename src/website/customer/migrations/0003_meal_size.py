# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0002_meal_combo'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='size',
            field=models.ForeignKey(to='customer.Size', null=True),
        ),
    ]
