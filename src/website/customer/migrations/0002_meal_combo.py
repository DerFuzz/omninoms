# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='meal',
            name='combo',
            field=models.ForeignKey(to='customer.Combo', null=True),
        ),
    ]
