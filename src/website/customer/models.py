from django.db import models
from dispatcher.models import Franchise, Order
import random
import string


# Create your models here.
class Item(models.Model):
    franchise = models.ForeignKey(Franchise, related_name='menu')

    name = models.CharField(max_length=256)
    description = models.CharField(max_length=512)
    base_price = models.FloatField()
    img_url = models.CharField(max_length=256)

    ENTREE = 0
    BEVERAGE = 1
    SIDE = 2
    DESERT = 3
    CATEGORY_CHOICES = (
                (ENTREE, 'Entree'),
                (BEVERAGE, 'Beverage'),
                (SIDE, 'Side'),
                (DESERT, 'Desert'),
            )
    category = models.IntegerField(choices=CATEGORY_CHOICES)

    def get_price(self):
        return (self.base_price +
                sum(self.options.filter(selected=True).values_list('price_change', flat=True)))


class Size(models.Model):
    item = models.ForeignKey(Item, related_name='sizes')
    name = models.CharField(max_length=256)
    price_change = models.FloatField()


class Variant(models.Model):
    item = models.ForeignKey(Item, related_name='variants')
    name = models.CharField(max_length=256)
    price_change = models.FloatField()


class Option(models.Model):
    item = models.ForeignKey(Item, related_name='options')
    price_change = models.FloatField()
    name = models.CharField(max_length=256)
    selected = models.BooleanField(default=False)


class Combo(models.Model):
    name = models.CharField(max_length=256)
    main = models.ForeignKey(Item, related_name='combos')
    add_ons = models.ManyToManyField(Item, related_name='combo_add_ons')
    price_change = models.FloatField()


class Meal(models.Model):
    """ This is a general purpose collection of menu items
    that a user can create. """
    order = models.ForeignKey(Order, related_name='meals')
    items = models.ManyToManyField('Item', related_name='meals')
    combo = models.ForeignKey('Combo', null=True)
    size = models.ForeignKey('Size', null=True)
    quantity = models.IntegerField(default=1)
    requests = models.CharField(max_length=512)
    description = models.CharField(max_length=512)

    def total(self):
        return sum([item.get_price() for item in self.items.all()])


class Special(Meal):
    """ These are restaurant specified arrangements of food that have
    some sort of special meaning (most likely a discount through some
    sort of combo) """
    restaurant = models.ForeignKey(Franchise, related_name='specials')
    price = models.FloatField()


class Invite(models.Model):
    email = models.CharField(max_length=256)
    code = models.CharField(max_length=50)

    def get_code(email):
        if Invite.objects.filter(email=email).count() != 0:
            return Invite.objects.get(email=email).code

        invite = Invite.objects.create(code=''.join(random
                                                    .SystemRandom()
                                                    .choice(
                                                      string.ascii_uppercase +
                                                      string.digits)
                                                    for _ in range(50)),
                                       email=email)

        return invite.code
