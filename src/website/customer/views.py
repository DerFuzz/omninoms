import json
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from django.template.loader import get_template
from django.contrib.auth.hashers import make_password
from django.core.mail import send_mail

from dispatcher.models import Customer, Order, Franchise, Location, OpenHours
from customer.models import Item, Meal, Invite, Combo, Size
from dispatcher.views import submit_order

from dispatcher.logic.geocoder import Geocoder
from traceback import print_exc


def restrict_hours(f, *args):
    def wrapped_args(*args, **kwargs):
        if not OpenHours.open():
            open, close = OpenHours.getHoursString()
            msg = "Sorry we're closed right now!\nOur current hours are from {0} to {1}.".format(open, close)
            return template_response('errors.html', args[0], {'error': msg})

        return f(*args, **kwargs)
    return wrapped_args

                
def template_response(template, request, context={}):
    return HttpResponse(get_template(template)
                        .render(gen_context(request, context)))


def message(request, msg):
    return template_response('errors.html', request, {'error': msg})


def gen_context(request, context={}):
    if request.user.is_authenticated():
        context['logged_in'] = True
    else:
        context['logged_in'] = False

    tray = request.session.get('tray')

    if tray:
        if 'total' in tray:
            context['tray_total'] = tray['total']
        if 'total_str' in tray:
            context['tray_total_str'] = tray['total_str']
    else:
        context['tray_total'] = False

    return RequestContext(request, context)


def test(request):
    return template_response('index.html', request)


def about(request):
    template = loader.get_template('about.html')
    return HttpResponse(template.render(gen_context(request)))


def hours(request):
    template = loader.get_template('hours.html')
    return HttpResponse(template.render(gen_context(request)))


def faq(request):
    template = loader.get_template('faq.html')
    return HttpResponse(template.render(gen_context(request)))


def mcdonalds(request, category):
    template = loader.get_template('mcdonalds.html')

    return HttpResponse(template.render(gen_context(request,
                                                    {"items": []})))


def menu(request, restaurant, category):
    template = loader.get_template('items.html')

    cfilter = -1
    for cid, desc in Item.CATEGORY_CHOICES:
        if category == desc.lower():
            cfilter = cid

#    menu_items = [(i, i.variants.all()) for i in Item.objects.filter(franchise__name='Mc Donalds',
#                                                 category=cfilter)]
    menu_items = []
    for item in Item.objects.filter(franchise__name='Mc Donalds',
                                    category=cfilter):
        combos = []
        for c in item.combos.all():
            combos.append((
                c,
                [], # [(add_on, add_on.variants.all()) for add_on in c.add_ons.all()]
                ))

        menu_items.append((item, combos, [size for size in item.sizes.all()]))
        
    return HttpResponse(template.render(RequestContext(request,
                                        {"items": menu_items})))


def tray(request):
    if request.method == 'POST' and not 'delete' in request.POST:
        tray = request.session.setdefault('tray', {'items': []})
        tray['items'].append((
            request.POST['id'],
            request.POST['combo'] if 'combo' in request.POST else None,
            request.POST['size'] if 'size' in request.POST else None,
            int(request.POST['qty']),
            request.POST['request']
        ))
        tray['total'] = float(request.POST['total'])
        tray['total_str'] = request.POST['total_str']
        request.session['tray'] = tray

        return HttpResponse(json.dumps(tray))
    if request.method == 'POST' and 'delete' in request.POST:
        tray = request.session.setdefault('tray', {'items': []})
        index = int(request.POST['index'])

        try:
            del tray['items'][index]
        except Exception as e:
            print(e)

        subtotal = 0
        for id, combo, size, qty, _ in tray['items']:
            item = Item.objects.get(id=id)
            if combo is None:
                subtotal += item.base_price * qty
            else:
                combo = Combo.objects.get(id=combo)
                subtotal += (item.base_price * combo.price_change) * qty
            if size is None:
                subtotal += item.base_price * qty
            else:
                size = Size.objects.get(id=size)
                subtotal += (item.base_price + size.price_change) * qty

        tray['total'] = subtotal
        tray['total_str'] = round(subtotal, 2)
        request.session['tray'] = tray

        if len(tray['items']) == 0:
            del request.session['tray']

    return HttpResponse(json.dumps(request.session.get('tray')))


@login_required(login_url='/login')
def add_location(request):
    if request.method == 'POST':
        address = request.POST['address']
        city = request.POST['city']
        instructions = request.POST['instructions']

        ok, lat, lng = check_address(address + " " + city + " Ontario")

        if ok:
            customer = request.user.customer_account
            if customer.locations.filter(address=address, instructions=instructions).count() != 0:
                return HttpResponse('{"okay": false, "detail": "That address already exists!"}')
            l = Location.objects.create(lat=lat,
                                        lng=lng,
                                        address=address,
                                        city=city,
                                        instructions=instructions)
            customer.locations.add(l)
            customer.save()
            return HttpResponse('{"okay": true, "id": ' + str(l.id) + '}')
        else:
            return HttpResponse('{"okay": false, "detail": "Sorry! We currently only deliver to Windsor & Tecumseh Addresses :("}') 

    return message("Problem :(")


@login_required(login_url='/login')
@restrict_hours
def checkout(request):
    tray = request.session.get('tray')
    context = {'items': []}

    if tray is None:
        return template_response('errors.html',
                                 request,
                                 {'error': 'You have no items in your tray!'})

    sub_total = 0.0
    for id, combo, size, qty, req in tray['items']:
        item = Item.objects.get(id=id)
        price = qty * item.base_price
        if Combo.objects.filter(id=combo).count() != 0:
            combo = Combo.objects.get(id=combo)
            price = qty * (item.base_price + combo.price_change)
        if size is not None:
            size = Size.objects.get(id=size)
            price = qty * (item.base_price + size.price_change)

        context['items'].append((item,
                                 combo,
                                 size,
                                 qty,
                                 round(price, 2),
                                 req))
        sub_total += price

    context['delivery_fee'] = 6.0
    context['taxes'] = round((sub_total + context['delivery_fee']) * 0.13, 2)
    context['grand_total'] = round(context['taxes'] + sub_total + context['delivery_fee'], 2)
    context['locations'] = list(request.user.customer_account.locations.all())

    return template_response('checkout.html', request, context)


@login_required(login_url='/login')
@restrict_hours
def pay(request):
    tray = request.session.get('tray')
    if tray is None:
        return template_response('errors.html',
                                 request,
                                 {'error': 'You have no items in your tray!'})

    try:
        customer = Customer.objects.get(user__username=request.user.username)
    except Exception as e:
        print(e)
        return template_response('errors.html',
                                 request,
                                 {'error': 'Not a customer!'})

    o = Order.objects.create(location=Location.objects.get(id=request.POST['location']),
                             franchise=Franchise.objects.all()[0])
    meals = []
    for id, combo, size, qty, req in tray['items']:
        m = Meal.objects.create(order=o, requests=req)
        item = Item.objects.get(id=id)
        m.items.add(item)

        if combo is not None:
            m.combo = Combo.objects.get(id=combo)
            combo = m.combo.name
        else:
            combo = ""
            if item.combos.all().count() != 0:
                m.combo = item.combos.all()[0]
                combo = m.combo.name
        if size is not None:
            m.size = Size.objects.get(id=size)
            size = m.size.name
        else:
            size = ""
            if item.sizes.all().count() != 0:
                m.size = item.sizes.all()[0]
                size = m.size.name

        m.quantity = qty
        m.requests = req
        m.save()
        meals.append("{4} {0} {3} x{1}  {2}".format(Item.objects.get(id=id).name,
                                            m.quantity,
                                            m.requests,
                                            combo,
                                            size))

    customer.orders.add(o)
    customer.save()
    o.save()
    del request.session['tray']

    try:
        submit_order(customer, o)
        if settings.DEBUG == False:
            send_mail('[omninoms.com][order] #{0}'.format(o.id),
                      ("Name: {0} {1}\n".format(customer.user.first_name,
                                                customer.user.last_name) +
                       "Address: {0}, {1}\n\n".format(o.location.address,
                                                      o.location.city) + 
                       "Franchise: {0}\n".format(o.franchise.name) +
                       "Meals: " + "\n".join(meals)),
                      "admin@omninoms.com", ['jon@omninoms.com',
                                             'steve@omninoms.com',
                                             'samantha@omninoms.com'])

    except:
        return template_response('errors.html', request, {'error': 'An error occured :('})

    return template_response('pay.html', request)


@login_required(login_url='/login')
def orders(request):

    try:
        orders = []

        for order in Customer.objects.get(
                       user__username=request.user.username).orders.all():
            items = []
            for meal in order.meals.all():
                for item in meal.items.all():
                    items.append((item, meal.quantity, meal.requests))

            orders.append((order.id, items))

        return template_response('orders.html', request, {'orders': orders})
    except Exception as e:
        print(e)
        return template_response('errors.html',
                                 request,
                                 {'errors':
                                  'Woops! Something went wrong there.'})


def check_address(address):
    try:
        lng, lat = Geocoder().get_location(address)
    except Exception as e:
        print(e)
        print("Geocoder error")
        print_exc()

    print('coords')
    print(lat, lng)

    if lat > 42.3421 or lat < 42.2300 or lng > -82.8664 or lng < -83.0875:
        return (False, lat, lng)

    return (True, lat, lng)


def signup(request, code):
    """
    if code == '' or Invite.objects.filter(code=code).count() == 0:
        return template_response('errors.html',
                                 request,
                                 {'error': 'Sorry, registration is currently by invite only!'})
    """

    if request.method == 'GET':         
        template = loader.get_template('signup.html')
        return HttpResponse(template.render(gen_context(request, {'code': code})))

    if request.method == 'POST':
        password = request.POST['password']
        password2 = request.POST['password2']
        email = request.POST['email']
        first = request.POST['firstname']
        last = request.POST['lastname']
        phone = request.POST['phonenumber']
        address = request.POST['address']
        city = request.POST['city']
        province = request.POST['province']
        print(address)

        if password != password2:
            return message(request, 'Passwords don\'t match.')

        ok, lat, lng = check_address(address + " " + city + " " + province)

        if not ok:
            return message(request, "We're currently only offering service in the Windsor area - Sorry :(")

        try:
            user = User.objects.create(username=email,
                                       password=make_password(password),
                                       first_name=first,
                                       last_name=last,
                                       email=email)
            l=Location.objects.create(lat=lat,
                                      lng=lng,
                                      address=address,
                                      city=city,
                                      province=province)
            c= Customer.objects.create(user=user,
                                       phone=phone)
            c.locations.add(l)
            c.save()

            if settings.DEBUG == False:
                send_mail("[omninoms.com][registration] {0}".format(email),
                          ("A new user registered with the following credentials...\n\n" +
                           "Name: {0} {1}\n".format(first, last) +
                           "Email: {0}\n".format(email) + 
                           "Phone: {0}\n".format(phone) + 
                           "Address: {0}, {1}\n".format(address, city)),
                           "admin@omninoms.com", ['jon@omninoms.com',
                                                  'steve@omninoms.com',
                                                  'samantha@omninoms.com'])

            # Invite.objects.get(code=code).delete()
        except Exception as e:
            print(e)
            try:
                if 'user' in locals():
                    user.delete()
                if 'l' in locals():
                    l.delete()
                if 'c' in locals():
                    c.delete()
            except:
                pass


            print_exc()
            print(e)

            return message(request, 'An error occured, please try again.')
    
        return HttpResponseRedirect('/') 

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


def login_view(request):
    if request.method == 'GET':
        if 'next' in request.GET:
            context = {'redirect': request.GET['next']}
        else:
            context = {'redirect': '/'}

        return template_response('login.html', request, context)
    if request.method == 'POST':
        try:
            username = request.POST['username']
            password = request.POST['password']
            redirect = request.POST['redirect']

            user = authenticate(username=username, password=password)

            if user is None:
                return template_response('login.html',
                                         request,
                                         {'error':
                                          'Could not login using that \
                                           username and password.'})

            login(request, user)
            return HttpResponseRedirect(redirect)
        except Exception as e:
            print(e)
            return HttpResponseRedirect('/')

    return HttpResponse('Problem')


def contact(request):
    if request.method == 'GET':
        return template_response('contact.html', request, {})
    if request.method == 'POST':
        message = request.POST['message']
        name = request.POST['name'] 
        email = request.POST['email']
        send_mail('[omninoms.com][contact] Message from {0}'.format(name),
                  'The following messag was sent from the website contact us page:\n\n' +
                  message,
                  email,
                  ['jon@omninoms.com',
                   'steve@omninoms.com',
                   'samantha@omninoms.com'])

        return template_response('errors.html', request, {'error': 'Thank you! We\'ll get back to you as soon as we can :)'})

def crash_me(request):
    raise Exception("Testing error system")
