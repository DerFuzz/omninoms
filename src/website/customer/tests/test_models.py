from django.test import TestCase

from dispatcher.models import Restaurant, Location, Franchise
from customer.models import Item, Meal, Variant, Option


class TestMeal(TestCase):
    def test_total(self):
        mcds = Franchise.objects.create(name='Mc Donalds')
        restaurant = Restaurant.objects.create(location=Location.objects.create(),
                                               franchise=mcds)
        burger = Item.objects.create(franchise=mcds,
                                     name='Big Mac',
                                     description="Mc Donald's signature sandwich.",
                                     base_price=5.0,
                                     category=Item.ENTREE)
        coke = Item.objects.create(franchise=mcds,
                                   name='Coca Cola',
                                   description="The classic soda.",
                                   base_price=1.0,
                                   category=Item.BEVERAGE)

        combo = Meal.objects.create()
        combo.items = [burger, coke]
        combo.save()

        self.assertEqual(combo.total(), 6.0)

    def test_variant(self):
        mcds = Franchise.objects.create(name='Mc Donalds')

        pop = Item.objects.create(franchise=mcds,
                                  name='Pop',
                                  description="It's sugary and watery.",
                                  base_price=1.0,
                                  category=Item.BEVERAGE)

        Variant.objects.create(item=pop, name='Grape')
        Variant.objects.create(item=pop, name='Coke', default=True)

        self.assertEqual(len(pop.variants.all()), 2)

    def test_option(self):
        mcds = Franchise.objects.create(name='Mc Donalds')
        restaurant = Restaurant.objects.create(location=Location.objects.create(),
                                               franchise=mcds)

        pop = Item.objects.create(franchise=mcds,
                                  name='Pop',
                                  description="It's sugary and watery.",
                                  base_price=1.0,
                                  category=Item.BEVERAGE)

        Option.objects.create(item=pop,
                              name='Extra cheese',
                              price_change=0.7)
        Option.objects.create(item=pop,
                              name='More cowbell',
                              price_change=-0.5,
                              selected=True)

        self.assertEqual(len(pop.options.all()), 2)
        self.assertEqual(pop.get_price(), 0.5)
