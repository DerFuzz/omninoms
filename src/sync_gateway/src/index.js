var app = require('express')();
var https = require('https');
var fs = require('fs');
var http = require('http').Server(app);
var server = https.createServer({key: fs.readFileSync('/home/ec2-user/starbuck-csr/starbuck.key'), cert: fs.readFileSync('/home/ec2-user/starbuck-csr/starbuck.omninoms.com.crt'), passphrase: 'startupjams'});
server.listen(8080);
var io = require('socket.io').listen(server);
var request = require('request-promise');

var clients = [];

var API_SERVER = 'omninoms.com';

app.get('/notify/:username', function(req, res) {
  if(req.params.username in clients){
    console.log('Notified driver ' + req.params.username);
    clients[req.params.username].emit('notification', '');
    res.send('Notified driver ' + req.params.username);
  }
  else {
    res.status(404);
    res.send('Driver not found.');
  }
});

io.on('connection', function(socket) {
  console.log('a user connected');

  socket.on('register', function(data) {
    var options = {
      uri: 'https://' + API_SERVER + '/rest-auth/user/',
      headers: {
        Authorization: data.token
      }
    };

    request(options)
      .then((response) => {
        console.log('User registered.');
        clients[data.user] = socket;
        socket.emit('auth-ok', '');
      })
      .catch((error) => {
        console.log(error);
        console.log('User auth failed.');
        socket.emit('auth-failed', error);
      });
  });
});

http.listen(3001, function() {
  console.log('listening on 3001 & 8080');
});
