var gulp = require('gulp');
var mocha = require('gulp-mocha');
var babel = require('gulp-babel');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var del = require('del');

gulp.task('compile-js', function () {
  return gulp.src('src/**/*js')
    .pipe(babel())
    .pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
  gulp.watch(['src/**/*'], ['compile-js']);
});

gulp.task('default', ['clean', 'compile-js']);

gulp.task('clean', function () {
  del(['dist']);
});


gulp.task('test', function() {
  return gulp.src('src/**/test*js', {read: false})
    .pipe(mocha({reporter: 'nyan'}));
});
